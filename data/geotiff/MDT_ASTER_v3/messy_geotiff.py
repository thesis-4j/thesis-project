import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from pysheds.grid import Grid
import seaborn as sns
import warnings
from skimage import io, util, transform
import rasterio as rio
import json
import geopandas as gpd



warnings.filterwarnings('ignore')




grid = Grid.from_raster('./N37_38W8_9_crop.tif', data_name='dem')
dirmap = (64, 128, 1, 2, 4, 8, 16, 32)

fig, ax = plt.subplots(figsize=(8,8))
plt.imshow(grid.view('dem'), zorder=1, cmap='terrain')
ax.set_yticklabels([])
ax.set_xticklabels([])
plt.title('Raw DEM of region', size=14)
plt.show()


# Detect pits
depressions = grid.detect_depressions('dem')

# Plot pits
#fig, ax = plt.subplots(figsize=(8,8))
#ax.imshow(depressions, cmap='cubehelix', zorder=1)
#ax.set_yticklabels([])
#ax.set_xticklabels([])
#plt.title('Depressions', size=14)
#plt.show()


# Fill depressions
grid.fill_depressions(data='dem', out_name='flooded_dem')
# Detect flats
flats = grid.detect_flats('flooded_dem')

# Plot flats
fig, ax = plt.subplots(figsize=(8,8))
plt.imshow(flats, cmap='cubehelix', zorder=1)
ax.set_yticklabels([])
ax.set_xticklabels([])
plt.title('Flats', size=14)
plt.show()

# Attempt to correct flats
grid.resolve_flats(data='flooded_dem', out_name='inflated_dem')
# Detect remaining flats
flats = grid.detect_flats('inflated_dem')
# Compute flow direction based on corrected DEM
grid.flowdir(data='inflated_dem', out_name='dir', dirmap=dirmap)
# Compute flow accumulation based on computed flow direction
grid.accumulation(data='dir', out_name='acc', dirmap=dirmap) #pad=True?


# Delineate catchment at point of high accumulation
y, x = np.unravel_index(np.argsort(grid.acc.ravel())[-2], grid.acc.shape)
grid.catchment(x, y, data='dir', out_name='catch',
               dirmap=dirmap, xytype='index')






# Plot accumulation and catchment
pnet=np.where(~flats, grid.view('acc') + 1, np.nan)
fig, ax = plt.subplots(2, 2, figsize=(16, 16))
ax[0,0].imshow(pnet, zorder=1, cmap='plasma')
ax[0,1].imshow(np.where(~flats, grid.view('acc') + 1, np.nan), zorder=1, cmap='plasma',
               norm=colors.LogNorm(vmin=1, vmax=grid.acc.max()))
ax[1,0].imshow(np.where(grid.catch, grid.catch, np.nan), zorder=1, cmap='plasma')
ax[1,1].imshow(np.where(grid.catch, grid.view('acc') + 1, np.nan), zorder=1, cmap='plasma',
               norm=colors.LogNorm(vmin=1, vmax=grid.acc.max()))

ax[0,0].set_title('Accumulation (Linear Scale)', size=14)
ax[0,1].set_title('Accumulation (Log Scale)', size=14)
ax[1,0].set_title('Largest catchment', size=14)
ax[1,1].set_title('Largest catchment accumulation (Log Scale)', size=14)

for i in range(ax.size):
    ax.flat[i].set_yticklabels([])
    ax.flat[i].set_xticklabels([])

plt.show()


try:
  branches = grid.extract_river_network('catch', grid.acc, threshold=500, dirmap=dirmap)
except Exception as e:
  print(e)


for branch in branches['features']:
  line = np.asarray(branch['geometry']['coordinates'])
  plt.plot(line[:, 0], line[:, 1])

plt.show()



##### Save to file #####

def saveDict(dic,file):
    f = open(file,'w')
    f.write(str(dic))
    f.close()
#save geojson as separate file
saveDict(branches,'watercourse_WGS84.geojson')

######
# Save river network  to geojson
"""
imgz1 = pnet.astype(np.uint8)
results = ({
        'type': 'Feature',
        'properties': {'raster_val': v},
        'geometry': s }
    for i, (s, v)
        in enumerate(list(rio.features.shapes(imgz1, mask=None, transform=grid.affine))))

collection = {
    'type': 'FeatureCollection',
    'features': list(results) }

with open('network.json', 'w') as dst:
    json.dump(collection, dst)

"""

######


###### Burn DEM ########

# Set elevation change for burned cells
dz = 5.5

# Select cells with > 100 accumulation
mask = grid.view('acc') > 100

# Create a view onto the DEM array
dem = grid.view('dem', dtype=np.float64, nodata=np.nan)

# Subtract dz where accumulation is above the threshold
dem[mask] -= dz

# Plot results
fig, ax = plt.subplots(1, 2, figsize=(14,6))
ax[0].imshow(grid.view('dem', nodata=np.nan), cmap='terrain', zorder=1, extent=grid.extent)
ax[1].imshow(dem, cmap='terrain', zorder=1, extent=grid.extent)

ax[0].set_title('Original DEM', size=14)
ax[1].set_title('Burned DEM', size=14)

ax[0].set_ylabel('Latitude')
ax[0].set_xlabel('Longitude')
ax[0].set_xlabel('Longitude')
plt.show()



