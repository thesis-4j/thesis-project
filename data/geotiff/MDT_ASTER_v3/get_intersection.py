import random
import json
import geopandas as gpd
import pandas as pd
from geojson import Polygon, Feature, FeatureCollection, Point
import warnings
import osmnx as ox

import rasterio
from pyproj.crs import CRS
from pyproj.enums import WktVersion
from distutils.version import LooseVersion
import earthpy.spatial as es
from pysheds.grid import Grid

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import fiona
from shapely.geometry import shape, GeometryCollection

from matplotlib.patches import ConnectionPatch


dataset= rasterio.open('./crop/N37_38W8_9_crop_-7.3970_38.3424.tif')
print(dataset.crs, dataset.transform)
print("All info:",dataset.meta)
print("number of bands: ", dataset.indexes)	# check availabe band
band1 = dataset.read(1) # choose band - only have 1 and it's height

df = gpd.read_file("./crop/N37_38W8_9_crop_-7.3970_38.3424.geojson")
def id(row):
	return 'F'+ str(row.name)

df['name']=df.apply(lambda x: id(x), axis=1)
print(df.head())



with open("./crop/N37_38W8_9_crop_-7.3970_38.3424.geojson", "rb") as read_file:
   gdata = json.load(read_file)


# CONVERT TO SHAPEFILE ERSI
#features = gdata['features']
#shp = GeometryCollection([shape(feature["geometry"]).buffer(0) for feature in features])



for geom in gdata['features']:
	h = []
	for coord in geom["geometry"]["coordinates"]:
		row, col = dataset.index(coord[0],coord[1])
		h.append(band1[row,col])

	minval =min(h)
	maxval = max(h)
	imax = h.index(maxval)
	imin = h.index(minval)
	length = len(h)


	#print(minval, maxval,geom["geometry"]["coordinates"][imin], geom["geometry"]["coordinates"][imax] ,geom["geometry"]["coordinates"][0], geom["geometry"]["coordinates"][-1])

	mid = length/2
	if(imax > mid):
		rimax=-1
		maxPoint = geom["geometry"]["coordinates"][-1]
	elif (imax < mid):
		rimax=-0
		maxPoint = geom["geometry"]["coordinates"][0]
	else:
		rimax=imax
		maxPoint = geom["geometry"]["coordinates"][imax]

	if(imin < mid):
		rimin=0
		minPoint = geom["geometry"]["coordinates"][0]
	elif (imin > mid):
		rimin=-1
		minPoint = geom["geometry"]["coordinates"][-1]
	else:
		rimin=imin
		minPoint = geom["geometry"]["coordinates"][imin]

	geom["properties"]["path"]= [[maxPoint,minPoint]]
	geom["properties"]["flow"]="downstream"
	geom["properties"]["index"]=[imax,imin]
	geom["properties"]["relative_index"]=[rimax,rimin]

	if (rimax == rimin):
		geom["properties"]["flat"]=1

	else:
		geom["properties"]["flat"]=0






##### GET COORDS FROM GEODATAFRAME

"""
def coord(row):
	return row.iloc[0].coords

arrows = gpd.GeoDataFrame()
arrows['coord']=df.apply(lambda x: coord(x), axis=1)


#tupleCoord = df.geometry.apply(lambda geom: geom.coords)
#print(tupleCoord)

"""


#### MANIPULATED COORDINATES
"""
def flow_dir(rows):
	h = []
	lrow = list(rows)

	for coord in lrow:
		row, col = dataset.index(coord[0],coord[1])
		h.append(band1[row,col])

	minval =min(h)
	maxval = max(h)

	imax = h.index(maxval)
	imin = h.index(minval)

	length = len(h)


	if(imax > length/2):
		maxPoint = lrow[-1]
	else:
		maxPoint = lrow[imax]

	if(imin < length/2):
		minPoint= lrow[0]
	else:
		minPoint = lrow[imin]

	return ["downstream" ,[maxPoint, minPoint]]


arrows['flow']=arrows['coord'].apply(flow_dir)
print(arrows['flow'])
"""




def get_intersections(gdf, name, geom = 'geometry', crs = 4326):
    intersections_gdf = pd.DataFrame()
    name_2 = '{}_2'.format(name)
    crs_init = {'init': 'epsg:{}'.format(crs)}
    for index, row in gdf.iterrows():
        row_intersections = gdf.intersection(row[geom])
        row_intersection_points = row_intersections[row_intersections.geom_type == 'Point']
        row_intersections_df = pd.DataFrame(row_intersection_points)
        row_intersections_df[name_2] = row[name]
        row_intersections_df = row_intersections_df.join(gdf[name])
        intersections_gdf = row_intersections_df.append(intersections_gdf)

    intersections_gdf = intersections_gdf.rename(columns={0: geom})
    intersections_gdf['intersection'] = intersections_gdf.apply(lambda row: '-'.join(sorted([row[name_2], row[name]])), axis = 1)
    intersections_gdf = intersections_gdf.groupby('intersection').first()
    intersections_gdf = intersections_gdf.reset_index()
    intersections_gdf = gpd.GeoDataFrame(intersections_gdf, geometry = 'geometry')
    intersections_gdf.crs = crs_init

    return intersections_gdf


intersect = get_intersections(df, 'name')
#print(intersect.to_json())
#print(json.dumps(gdata))

#fig, ax = plt.subplots()


"""
get_intersections(df, 'name').plot(ax=ax, color='black')
df.plot(ax=ax, color='red')
plt.show();
"""




