import numpy as np
from matplotlib import pyplot as plt
from skimage import io, util
from pysheds.grid import Grid
import earthpy.spatial as es
import rasterio as rio
import geopandas as gpd
from geojson import Polygon, Feature, FeatureCollection
import json
from poly_crop import poly_crop, small_crop
import rasterio

from pyproj.crs import CRS
from pyproj.enums import WktVersion
from distutils.version import LooseVersion
from pyproj.enums import WktVersion
import fiona

catch =None

#Define a function to plot the digital elevation model
def plotFigure(data, label='', cmap='Blues'):
    plt.figure(figsize=(12,10))
    plt.imshow(data, extent=grid.extent)
    plt.colorbar(label=label)
    plt.grid()
    plt.show()

dirmap = (64,  128,  1,   2,    4,   8,    16,  32)

# read the image stack
img = io.imread('./N37_38W8_9.tif')
print(img)
# show the image
plt.subplot(1, 3, 1)
plt.imshow(img)
plt.axis('off')
plt.title('GeoTIFF Alentejo-Algarve')
plt.xticks([])
plt.yticks([])



# save the image
#plt.savefig('output.tif', transparent=True, dpi=300, bbox_inches="tight", pad_inches=0.0)
crop = Polygon([
            [
              -7.83599853515625,
              38.037275688165614
            ],
            [
              -7.152099609375,
              38.037275688165614
            ],
            [
              -7.152099609375,
              38.67050053364344
            ],
            [
              -7.83599853515625,
              38.67050053364344
            ],
            [
              -7.83599853515625,
              38.037275688165614
            ]
          ])

#print(crop)
#cropFeature = Feature(geometry=crop)

#print(cropFeature)
#fCollection = FeatureCollection([cropFeature])

#print(fCollection)


##### Alqueva crop #####

dfCrop = gpd.GeoDataFrame.from_features(poly_crop['features'])
print(dfCrop)

with rio.open('./N37_38W8_9.tif') as src_raster:
	cropped_raster, cropped_meta = es.crop_image(src_raster, dfCrop)

print(cropped_meta)

cropped_meta.update({"driver": "GTiff",
                 "height": cropped_raster.shape[0],
                 "width": cropped_raster.shape[1],
                 "transform": cropped_meta["transform"]})

plt.subplot(1, 3, 2)
plt.imshow(cropped_raster.squeeze())
plt.axis('off')
plt.title('GeoTIFF Alqueva')
plt.xticks([])
plt.yticks([])


##### sub Alqueva crop

dfCrop2 = gpd.GeoDataFrame.from_features(small_crop['features'])
print(dfCrop2)

with rio.open('./N37_38W8_9.tif') as src_raster:
	cropped_raster, cropped_meta = es.crop_image(src_raster, dfCrop2)

print(cropped_meta)

cropped_meta.update({"driver": "GTiff",
                 "height": cropped_raster.shape[0],
                 "width": cropped_raster.shape[1],
                 "transform": cropped_meta["transform"]})

plt.subplot(1, 3, 3)
plt.imshow(cropped_raster.squeeze())
plt.axis('off')
plt.title('GeoTIFF Sub - Alqueva')
plt.xticks([])
plt.yticks([])
plt.show()



# Instantiate grid from raster

#grid = Grid.from_raster('./N37_38W8_9.tif', data_name='dem')

## convert to rasterio CSR
#crs_rio = rio.crs.CRS.from_user_input(cropped_meta['crs'])
#print(crs_rio)

proj_crs = CRS.from_epsg(4326)
if LooseVersion(fiona.__gdal_version__) < LooseVersion("3.0.0"):
    fio_crs = proj_crs.to_wkt(WktVersion.WKT1_GDAL)
else:
    # GDAL 3+ can use WKT2
    fio_crs = dc_crs.to_wkt()





global grid
grid = Grid()
print(grid.crs)
grid.add_gridded_data(data=cropped_raster.squeeze(), data_name='dem',affine=cropped_meta['transform'], crs=fio_crs, nodata=cropped_meta['nodata'])

grid.to_raster('dem', 'N37_38W8_9_crop.tif', view=False)


dep = grid.detect_depressions('dem') #old grid.detect_nondraining_flats


plt.imshow(dep)
plt.axis('off')
plt.title('Alqueva Depressions')
plt.xticks([])
plt.yticks([])
plt.show()

# fill pits
grid.fill_depressions(data='dem', out_name='flooded_dem')
flats = grid.detect_flats('flooded_dem')
plt.imshow(flats)
plt.axis('off')
plt.title('Alqueva Flats')
plt.xticks([])
plt.yticks([])
plt.show()

#dep = grid.detect_depressions('dem') #Iteratively raise nondraining flats until all flats have a low edge cell
#dep = grid.detect_depressions('dem')
#dep = grid.detect_depressions('dem')
#dep = grid.detect_depressions('dem')



# Resolve flats and compute flow directions
grid.resolve_flats(data='flooded_dem', out_name='inflated_dem')
plt.imshow(grid.inflated_dem[:-1,:-1])
plt.axis('off')
plt.title('Alqueva Inflated')
plt.xticks([])
plt.yticks([])
plt.show()


grid.flowdir('inflated_dem', out_name='dir',dirmap=dirmap)
plotFigure(grid.dir,'Flow Direction','viridis')


##### catchement basins ######

grid.mask = np.ones_like(grid.mask).astype(bool)

# Generate random points

yi_s = np.random.randint(0, grid.shape[0], size=10)
xi_s = np.random.randint(0, grid.shape[1], size=10)
xs, ys = grid.affine * (xi_s, yi_s)

# Delineate catchments at random points
z0 = np.zeros(grid.shape)
i = 0

for x, y in zip(xs, ys):
    i += 1
    c = grid.catchment(x, y, data='dir', dirmap=dirmap, out_name='catchz', xytype='label', inplace=False)
    z0 += i * (c != 0).astype(int)

# Snap points to nearest high accumulation cells
grid.accumulation(data='dir', dirmap=dirmap, out_name='acc', apply_mask=False, pad=True)

xy = np.column_stack([xs, ys])
new_xy = grid.snap_to_mask(grid.acc > 50, xy, return_dist=False)
new_xs, new_ys = new_xy[:,0], new_xy[:,1]

# Delineate catchments at snapped points
z1 = np.zeros(grid.shape)
i = 0

for x, y in zip(new_xs, new_ys):
    i += 1
    c = grid.catchment(x, y, data='dir', dirmap=dirmap, xytype='label',out_name='catchz', inplace=False)
    catch=c
    z1 += i * (c != 0).astype(int)


######
# Save catchment basins geometries to geojson
imgz1 = z1.astype(np.uint8)
results = ({
        'type': 'Feature',
        'properties': {'raster_val': v},
        'geometry': s }
    for i, (s, v)
        in enumerate(list(rio.features.shapes(imgz1, mask=None, transform=cropped_meta['transform']))) )

collection = {
    'type': 'FeatureCollection',
    'features': list(results) }

with open('catchment_basins.json', 'w') as dst:
    json.dump(collection, dst)
######

# Plot results
fig, ax = plt.subplots(1, 2, figsize=(12, 6))

z0[z0 == 0] = np.nan
z1[z1 == 0] = np.nan

ax[0].imshow(z0, extent=grid.extent, zorder=1, cmap='plasma')
ax[0].scatter(xy[:,0], xy[:,1], marker='x', c='k', zorder=2)
ax[0].set_title('Catchments at original coordinates')
ax[0].set_xlabel('Longitude')
ax[0].set_ylabel('Latitude')

ax[1].imshow(z1, extent=grid.extent, zorder=1, cmap='plasma')
ax[1].scatter(new_xy[:,0], new_xy[:,1], marker='x', c='k', zorder=2)
ax[1].set_title('Catchments at snapped coordinates')
ax[1].set_xlabel('Longitude')

plt.tight_layout()
plt.show()


fig, ax = plt.subplots(figsize=(8,8))
ax.set_aspect('equal')

plt.scatter(xs, ys, marker='x', c='b', s=100, label='original')
plt.scatter(new_xs, new_ys, marker='x', c='r', s=100, label='snapped')
plt.legend(frameon=True)
plt.imshow(np.where(grid.acc > 50, 1, np.nan), extent=grid.extent, cmap='bone', zorder=1, alpha=0.5)
plt.title('Original vs. snapped coordinates')
plt.xlabel('Longitude')
plt.ylabel('Latitude')
plt.show()



try:
	branches = grid.extract_river_network(catch, grid.acc, threshold=2, dirmap=dirmap)
except Exception as e:
	print(e)


for branch in branches['features']:
	line = np.asarray(branch['geometry']['coordinates'])
	plt.plot(line[:, 0], line[:, 1])

plt.show()



##### Save to file #####

def saveDict(dic,file):
    f = open(file,'w')
    f.write(str(dic))
    f.close()
#save geojson as separate file
saveDict(branches,'streams_WGS84.geojson')
#streamNet = gpd.read_file('streams_WGS84.geojson')


