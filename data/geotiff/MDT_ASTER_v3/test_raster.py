import rasterio
import numpy as np
dataset= rasterio.open('./N37_38W8_9_crop.tif')
print(dataset.crs, dataset.transform)
print("All info:",dataset.meta)

## references
#https://rasterio.readthedocs.io/en/latest/quickstart.html
#

### get np index by geospatial location  using crs
y = 38.41306334236394
x = -7.4670352445555395
row, col = dataset.index(x, y)
print("row,col: ",row, col)

#### get raster value by index
print("number of bands: ", dataset.indexes)	# check availabe band
band1 = dataset.read(1) # choose band - only have 1 and it's height

print("height: ", band1[row,col])
#### convert raster np index to crs coordinates using affine transform
print("From idx to coords: ",dataset.transform * (y,x))
print("Raster tiff bounds: ", dataset.bounds)

#### Example #####
Feature = {
 	"geometry": {
 		"coordinates": [
 			[-7.461528, 38.429583],
 			[-7.46125, 38.429583],
 			[-7.460972, 38.429583],
 			[-7.460694, 38.429583],
 			[-7.460417, 38.429583],
 			[-7.460139, 38.429306],
 			[-7.459861, 38.429306],
 			[-7.459583, 38.429306],
 			[-7.459306, 38.429306],
 			[-7.459028, 38.429306],
 			[-7.45875, 38.429306],
 			[-7.458472, 38.429306],
 			[-7.458194, 38.429583],
 			[-7.458194, 38.429861],
 			[-7.458194, 38.430139],
 			[-7.458194, 38.430417],
 			[-7.458472, 38.430694],
 			[-7.458472, 38.430972],
 			[-7.458472, 38.43125],
 			[-7.45875, 38.431528],
 			[-7.458472, 38.431806],
 			[-7.458194, 38.432083],
 			[-7.457917, 38.432361],
 			[-7.457639, 38.432639],
 			[-7.457361, 38.432639],
 			[-7.457083, 38.432639],
 			[-7.456806, 38.432639],
 			[-7.456528, 38.432917]
 		],
 		"type": "LineString"
 	},
 	"id": 4,
 	"properties": {},
 	"type": "Feature"
 }


h = []
for coord in Feature["geometry"]["coordinates"]:
	row, col = dataset.index(coord[0],coord[1])
	h.append(band1[row,col])

minval =min(h)
maxval = max(h)
print("min (i=%f): " %  h.index(minval), minval,  h.count(minval))
print("max (i=%f): " % h.index(maxval), maxval, h.count(maxval))

print("start:",Feature["geometry"]["coordinates"][h.index(maxval)])
print("end:",Feature["geometry"]["coordinates"][h.index(minval)])






