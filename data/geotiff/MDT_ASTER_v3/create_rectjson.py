import random
import json
import geopandas as gpd
from geojson import Polygon, Feature, FeatureCollection, Point
import warnings

import rasterio as rio
from pyproj.crs import CRS
from pyproj.enums import WktVersion
from distutils.version import LooseVersion

import earthpy.spatial as es
from pysheds.grid import Grid

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import fiona


def saveDict(dic,file):
    f = open(file,'w')
    f.write(str(dic))
    f.close()

warnings.filterwarnings('ignore')

point = Point((-7.83599853515625, 38.037275688165614))
f = Feature(geometry=point)

df = gpd.GeoDataFrame.from_features([f])
df=df.buffer(0.07, cap_style=3) # cap_style=3 convert to square

#print(df.to_json())
#df.plot();
#plt.show();

minlat=  37.906517
minlng=  -7.373966

maxlat =38.577871
maxlng= -7.800412

sup_esq= (38.577871, -7.800412)
inf_dir = (37.906517, -7.373966)

dirmap = (64, 128, 1, 2, 4, 8, 16, 32)
points= []


for i in range(0,10):

    nlat= random.uniform( minlat, maxlat)
    nlng = random.uniform( minlng, maxlng)
    print("Polygon centroid:", nlat, nlng)

    points.append((nlng, nlat))

    point = Point((nlng, nlat))
    f = Feature(geometry=point)

    dff= gpd.GeoDataFrame.from_features([f])
    dfc = dff.buffer(0.06, cap_style=3)
    dfc = dfc.envelope

    print(dfc.to_json())

    dfc.to_file('./crop/N37_38W8_9_crop_%1.4f_%1.4f_polycrop.json' % (nlng,nlat), driver='GeoJSON')


    with rio.open('./N37_38W8_9.tif') as src_raster:
        cropped_raster, cropped_meta = es.crop_image(src_raster, dfc)

    cropped_meta.update({"driver": "GTiff",
                 "height": cropped_raster.shape[0],
                 "width": cropped_raster.shape[1],
                 "transform": cropped_meta["transform"]})

    proj_crs = CRS.from_epsg(4326)
    if LooseVersion(fiona.__gdal_version__) < LooseVersion("3.0.0"):
        fio_crs = proj_crs.to_wkt(WktVersion.WKT1_GDAL)
    else:
        fio_crs = dc_crs.to_wkt() # GDAL 3+ can use WKT2 datacube

    grid = Grid()
    grid.add_gridded_data(data=cropped_raster.squeeze(), data_name='dem',affine=cropped_meta['transform'], crs=fio_crs, nodata=cropped_meta['nodata'])

    grid.to_raster('dem', './crop/N37_38W8_9_crop_%1.4f_%1.4f.tif' % (nlng,nlat), view=False)
    del dff


for p in points:

    filename = './crop/N37_38W8_9_crop_%1.4f_%1.4f.tif' % (p[0],p[1])
    print("filename:",filename)
    grid = Grid.from_raster(filename, data_name='dem')

    """
    fig, ax = plt.subplots(figsize=(8,8))
    plt.imshow(grid.view('dem'), zorder=1, cmap='terrain')
    ax.set_yticklabels([])
    ax.set_xticklabels([])
    plt.title('Raw DEM of region', size=14)
    plt.show()
    """


    # Detect pits
    depressions = grid.detect_depressions('dem')

    # Plot pits
    #fig, ax = plt.subplots(figsize=(8,8))
    #ax.imshow(depressions, cmap='cubehelix', zorder=1)
    #ax.set_yticklabels([])
    #ax.set_xticklabels([])
    #plt.title('Depressions', size=14)
    #plt.show()


    # Fill depressions
    grid.fill_depressions(data='dem', out_name='flooded_dem')
    # Detect flats
    flats = grid.detect_flats('flooded_dem')

    # Plot flats
    #fig, ax = plt.subplots(figsize=(8,8))
    #plt.imshow(flats, cmap='cubehelix', zorder=1)
    #ax.set_yticklabels([])
    #ax.set_xticklabels([])
    #plt.title('Flats', size=14)
    #plt.show()

    # Attempt to correct flats
    grid.resolve_flats(data='flooded_dem', out_name='inflated_dem')
    # Detect remaining flats
    flats = grid.detect_flats('inflated_dem')
    # Compute flow direction based on corrected DEM
    grid.flowdir(data='inflated_dem', out_name='dir', dirmap=dirmap)
    # Compute flow accumulation based on computed flow direction
    grid.accumulation(data='dir', out_name='acc', dirmap=dirmap) #pad=True?


    # Delineate catchment at point of high accumulation
    y, x = np.unravel_index(np.argsort(grid.acc.ravel())[-2], grid.acc.shape)
    grid.catchment(x, y, data='dir', out_name='catch',
                   dirmap=dirmap, xytype='index')




    """

    # Plot accumulation and catchment
    pnet=np.where(~flats, grid.view('acc') + 1, np.nan)
    fig, ax = plt.subplots(2, 2, figsize=(16, 16))
    ax[0,0].imshow(pnet, zorder=1, cmap='plasma')
    ax[0,1].imshow(np.where(~flats, grid.view('acc') + 1, np.nan), zorder=1, cmap='plasma',
                   norm=colors.LogNorm(vmin=1, vmax=grid.acc.max()))
    ax[1,0].imshow(np.where(grid.catch, grid.catch, np.nan), zorder=1, cmap='plasma')
    ax[1,1].imshow(np.where(grid.catch, grid.view('acc') + 1, np.nan), zorder=1, cmap='plasma',
                   norm=colors.LogNorm(vmin=1, vmax=grid.acc.max()))

    ax[0,0].set_title('Accumulation (Linear Scale)', size=14)
    ax[0,1].set_title('Accumulation (Log Scale)', size=14)
    ax[1,0].set_title('Largest catchment', size=14)
    ax[1,1].set_title('Largest catchment accumulation (Log Scale)', size=14)

    for i in range(ax.size):
        ax.flat[i].set_yticklabels([])
        ax.flat[i].set_xticklabels([])

    plt.show()
    """

    try:
      branches = grid.extract_river_network('catch', grid.acc, threshold=500, dirmap=dirmap)
    except Exception as e:
      print(e)

    """
    for branch in branches['features']:
      line = np.asarray(branch['geometry']['coordinates'])
      plt.plot(line[:, 0], line[:, 1])

    plt.show()
    """


    #save geojson as separate file
    saveDict(branches,'./crop/N37_38W8_9_crop_%1.4f_%1.4f.geojson' % (p[0],p[1]))















