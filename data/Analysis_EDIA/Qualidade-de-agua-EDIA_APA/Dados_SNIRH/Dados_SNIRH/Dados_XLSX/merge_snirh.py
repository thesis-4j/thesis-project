import pandas as pd
import glob

path='/home/bmalbusca/Documents/IST/thesis-4j/thesis-project/data/Analysis_EDIA/Qualidade-de-agua-EDIA_APA/Dados_SNIRH/Dados_SNIRH/Dados_XLSX'

all_files = glob.glob(path + "/WaterQualityStations-*")
li= []

for filename in all_files:
    df = pd.read_csv(filename, index_col=None, header=0)
    li.append(df)

data = pd.concat(li, axis=0, ignore_index=True)
print(data.shape, len(li))

stations=data.drop_duplicates(subset=['snirh'], inplace=False)
print(len(data['snirh'].unique()),stations.shape)


def id(row):
    return "WQS_SNIRHSUP_" + str(row.name)

stations['id']=stations.apply(lambda x: id(x), axis=1)

print(stations)
stations.to_csv('WaterQualityStations_SNIRHQUALSUP.csv', index=False)
