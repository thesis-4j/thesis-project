import sys
import pandas as pd
#WQS_SNIRHSUP1516_ WQS_SNIRHSUP19_ WQS_SNIRHSUP18_ WQS_SNIRHSUP17_ WQS_SNIRHSUP1214_ WQS_SNIRHSUP0911_
def id(row):
    return "WQS_SNIRHSUP0911_" + str(row.name)

# codigo -> SNIRH, latitude, longitude
sheet='snirh_qualsup_2009_2011'
filename='snirh_qualsup_2009_2011.xlsx'

df = pd.read_excel(filename, sheet_name=sheet, engine='openpyxl')
filter = df.iloc[:,1:15]
stations=filter#.drop_duplicates(subset=['codigo'])
stations['codigo']=stations['codigo'].astype('str')
stations['codigo']=stations['codigo'].apply(lambda x: x if len(x)<7 else x[:-1])
unique_station=stations.drop_duplicates(subset=['codigo'], inplace=False)
#unique_station=stations
print("unique values:",len(stations['codigo'].unique()))
#unique_station['codigo']=unique_station['codigo'].unique()
unique_station.rename({'codigo':'snirh'},axis=1,inplace=True)
lat=unique_station.pop('latitude')
lng=unique_station.pop('longitude')
unique_station.insert(0,'id',"")
unique_station.insert(2,'latitude',lat)
unique_station.insert(3,'longitude',lng)
unique_station['id']=unique_station.apply(lambda x: id(x), axis=1)
print(unique_station)
unique_station.to_csv('WaterQualityStations-SNIRHQUALSUP0911.csv', index=False)
