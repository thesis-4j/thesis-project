import sys
import pandas as pd
import datetime

cdata={ 'nov': '11', 'dez':'12', 'out':'10', 'set':'09', 'aug': '08', 'jan':'01', 'fev':'02', 'mar':'03', 'abr':'04' , 'mai':'05', 'jun':'06', 'jul':'07'}


filename='SNIRH_QUALSUB_2016_2019.xlsx'
sheet = filename.split(".")[0]
finalname='WaterQualityData-SNIRH_QUALSUB1619.csv'


filename_stations='/Users/bmalbusca/Documents/IST/Thesis-4j/thesis-project/data/Analysis_EDIA/WQS/WaterQualityStations_9MAR21.csv'
station= pd.read_csv(filename_stations, encoding='utf-8')


df = pd.read_excel(filename, sheet_name=sheet, engine='openpyxl')
#filter = df.iloc[:,18:] # para sups
filter = df.iloc[:,5:] # para subs
filter.rename({'CODIGO_ESTACAO':'snirh', 'DATA':'date'},axis=1, inplace=True)



def format(row):
    try:
        t1= row.split("-")
        t2= row.split('/')

        if(len(t2)==3):
            try:
                return t1[0]+"/"+cdata[t1[1].lower()]+"/"+t2[0]
            except:
                return  row
        elif(len(t1)==3):
            try:
                return t1[0]+"/"+cdata[t1[1].lower()]+"/"+t2[0]
            except:
                return  t1[0]+"/"+t1[1]+"/"+t2[0]
        else:
            pass

    except:
        return row.strftime("%d/%m/%Y")

    return row

filter['date']=filter['date'].apply(lambda x: format(x))
filter.fillna("")
print(filter)
filter.to_csv(finalname, index=False)
