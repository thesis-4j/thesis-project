import sys
import pandas as pd
import glob
from pyproj import Transformer
converter = Transformer.from_crs('epsg:20790','epsg:4326')

file_rede = "/home/bmalbusca/Documents/IST/thesis-4j/thesis-project/data/Analysis_EDIA/Qualidade-de-agua-EDIA_APA/Dados_SNIRH/Dados_SNIRH/Dados_XLSX/rede_Qualidadeaguassubterraneas.csv"
stations = pd.read_csv(file_rede, encoding='utf-8' ) # file -i <file> to know encoding

filename="SNIRH_QUALSUB_2000_2009.xlsx"
#sheet="SNIRH_QUALSUB_2000_2009"
#data = pd.read_excel(filename, sheet_name=sheet,  engine='openpyxl')
path="/home/bmalbusca/Documents/IST/thesis-4j/thesis-project/data/Analysis_EDIA/Qualidade-de-agua-EDIA_APA/Dados_SNIRH/Dados_SNIRH/Dados_XLSX"
all_files = glob.glob(path + "/SNIRH_QUALSUB*")

lst=[]
for filename in all_files:
    df = pd.read_excel(filename,engine='openpyxl')
    lst.append(df)

#print(lst)
data=pd.concat(lst, axis=0, ignore_index=True)
#print(data['referencia'].unique(), len(data['referencia'].unique()))



data.drop_duplicates(subset=['referencia'], inplace=True)
#print( stations["CODIGO"].size)
#print(data['referencia'].unique(), stations['CODIGO'].unique())
stations['CODIGO']=stations['CODIGO'].astype('str')
unique_stations=stations[stations['CODIGO'].isin(data['referencia'].unique())]
lat=unique_stations.pop('COORD_X (M)')
lng=unique_stations.pop('COORD_Y (M)')

unique_stations.rename({'CODIGO':'snirh'},axis=1,inplace=True)
unique_stations.insert(0,'id',"")
unique_stations.insert(2,'latitude',lat)
unique_stations.insert(3,'longitude',lng)
data=unique_stations.fillna('')

def id(row):
    return 'WQS_SNIRHSUB_'+ str(row.name)
def rule(s,row):
    lat, lon = converter.transform(row['latitude'],row['longitude'])
    if(s ==0):
        return lat
    else:
        return lon

data['id']=data.apply(lambda x: id(x), axis=1)
data['latitude']=data.apply(lambda x: rule(0,x), axis=1)
data['longitude']=data.apply(lambda x: rule(1,x), axis=1)

print(data)
data.to_csv('WaterQualityStations-SNIRHQUALSUB.csv', index=False)



