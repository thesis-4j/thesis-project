import sys
import pandas as pd
import datetime

cdata={ 'nov': '11', 'dez':'12', 'out':'10', 'set':'09', 'aug': '08', 'jan':'01', 'fev':'02', 'mar':'03', 'abr':'04' , 'mai':'05', 'jun':'06', 'jul':'07'}


filename_stations='WaterQualityStations-INAG1V2.csv'
filename_data='Sig_ERHSA_quimica.xlsx'

sheet_altas="Aguas Altas"
sheet_baixas="Aguas Baixas"

data_altas_df=pd.read_excel(filename_data, sheet_name=sheet_altas,  engine='openpyxl')
data_baixas_df=pd.read_excel(filename_data, sheet_name=sheet_baixas,  engine='openpyxl')
station= pd.read_csv(filename_stations, encoding='utf-8')

data_altas_df.rename({'N_ ERHSA':'ERHSA', 'Data_Amostra':'date'},axis=1, inplace=True)
data_baixas_df.rename({'N_Ordem':'ERHSA','Data_Amostra':'date'},axis=1, inplace=True)

def format(row):
    try:
        t1= row.split("-")
        t2= row.split('/')

        if(len(t2)==3):
            try:
                return t1[0]+"/"+cdata[t1[1].lower()]+"/"+t2[0]
            except:
                return  row
        elif(len(t1)==3):
            try:
                return t1[0]+"/"+cdata[t1[1].lower()]+"/"+t2[0]
            except:
                return  t1[0]+"/"+t1[1]+"/"+t2[0]
        else:
            pass

    except:
        return row.strftime("%d/%m/%Y")
    
    return row

data_altas_df['date']=data_altas_df['date'].apply(lambda x: format(x))
data_baixas_df['date']=data_baixas_df['date'].apply(lambda x: format(x))

data_altas = data_altas_df.merge(station.loc[:, ["id", "ERHSA"]], on=['ERHSA'], how='left')
data_baixas = data_baixas_df.merge(station.loc[:, ["id", "ERHSA"]], on=['ERHSA'], how='left')

idaltas = data_altas.pop('id')
data_altas.insert(0,'id',idaltas)
idbaixas = data_baixas.pop('id')
data_baixas.insert(0,'id',idbaixas)

#print(data_altas)

data_baixas.to_csv('WaterQualityData-SIGERSHAQUIMICA-Baixa.csv', index=False)
data_altas.to_csv('WaterQualityData-SIGERSHAQUIMICA-Alta.csv', index=False)




