import sys
import pandas as pd
import utm
from pyproj import Transformer
converter = Transformer.from_crs('epsg:20790','epsg:4326')


file_name ="originais_N_Ordem_enviar_INAG_1v2.xlsx"
sheet="Ponto de Água"
INAG_df = pd.read_excel(file_name, sheet_name=sheet,  engine='openpyxl')
#print(INAG_df.shape, "\n")
#print(INAG_df.columns, "\n")

stations = INAG_df.drop_duplicates(subset=['N_ ERHSA'])
print(stations.shape)


file_name2 ="Sig_ERHSA_quimica.xlsx"
sheet1="Aguas Altas"
quimica_alta_df = pd.read_excel(file_name2, sheet_name=sheet1,  engine='openpyxl')
#print(quimica_alta_df, "\n")
data_alta= quimica_alta_df.drop_duplicates(subset=['N_ ERHSA'])

sheet2="Aguas Baixas"
quimica_baixa_df = pd.read_excel(file_name2, sheet_name=sheet2,  engine='openpyxl')
#print(quimica_baixa_df,"\n")
data_baixa = quimica_baixa_df.drop_duplicates(subset=['N_Ordem'])
#print(data_baixa.columns.values)
#print(data_alta.columns.values)

baixa_codes = data_baixa.loc[:,['N_Ordem']].copy()
baixa_codes.rename({'N_Ordem':'N_ ERHSA'},axis=1, inplace=True)
alta_codes = data_alta.loc[:,['N_ ERHSA']].copy()
print("baixa=",baixa_codes.shape)
print("alta=",alta_codes.shape)
#diff = [x for x in INAG_df['N_ ERHSA'] if x in baixa_codes['N_ ERHSA']]
#que(print(diff) 

exists = []
counter =0
for x in INAG_df['N_ ERHSA']:
    for y in baixa_codes['N_ ERHSA']:
        #print("INAG=",x," baixa=",y)
        if x==y:
            exists.append(x)
            counter +=1
            #print("counter=",counter)

#print(len(list(set(exists))))

exists2 = []
#counter2 =0
for x in stations['N_ ERHSA']:
    for y in alta_codes['N_ ERHSA']:
        #print("INAG=",x," baixa=",y)
        if x==y:
            exists2.append(x)
            #counter2 +=1


exists.extend(exists2)
s=(list(set(exists)))
print(len(s))

data = stations[stations['N_ ERHSA'].isin(exists)]
data.rename({'N_ ERHSA':'ERHSA'},axis=1, inplace=True)
data.insert(0, "id", "")
data.insert(1,"snirh","")
data.insert(2,"latitude","")
data.insert(3,"longitude","")
data=data.fillna('')


def UTMrule(s,row):
    lat, lon = utm.to_latlon(row["X"], row["Y"], 29, 'T')
    if(s == 0):
        return lat
    elif (s ==1):
        return lon
    else:
        return ""

def rule(s,row):
    lat, lon = converter.transform(row['X'],row['Y'])
    if(s == 0):
        return lat
    elif (s ==1):
        return lon
    else:
        return ""

def id(row):
    return "WQS_INAG1_" + str(row.name)

data['latitude']=data.apply(lambda x: rule(0,x), axis=1)
data['longitude']=data.apply(lambda x: rule(1,x), axis=1)
data['id']=data.apply(lambda x: id(x), axis=1)
print(data)
data.to_csv('WaterQualityStations-INAG1V2.csv',index=False)


