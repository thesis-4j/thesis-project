import sys
import pandas as pd
import datetime

cdata={ 'nov': '11', 'dez':'12', 'out':'10', 'set':'09', 'aug': '08', 'jan':'01', 'fev':'02', 'mar':'03', 'abr':'04' , 'mai':'05', 'jun':'06', 'jul':'07'}


filename='Matriz_Rede_Secundaria_Sub.xlsx'
sheet = 'Parametros EDIA campanhas1_5'
finalname='WaterQualityData-MATRIZ_REDESECUND_SUB.csv'

df = pd.read_excel(filename, sheet_name=sheet, engine='openpyxl')

filter = df
filter.rename({'SNIRH':'snirh', 'Data':'date'},axis=1, inplace=True)


def format(row):
    try:
        t1= row.split("-")
        t2= row.split('/')

        if(len(t2)==3):
            try:
                return t1[0]+"/"+cdata[t1[1].lower()]+"/"+t2[0]
            except:
                return  row
        elif(len(t1)==3):
            try:
                return t1[0]+"/"+cdata[t1[1].lower()]+"/"+t2[0]
            except:
                return  t1[0]+"/"+t1[1]+"/"+t2[0]
        else:
            pass

    except:
        return row.strftime("%d/%m/%Y")

    return row


filter.fillna("")
print(filter)
filter.to_csv(finalname,index=False)
