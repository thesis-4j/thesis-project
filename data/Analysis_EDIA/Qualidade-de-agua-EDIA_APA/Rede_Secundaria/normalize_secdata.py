
import pandas as pd
import time
import resource
import datetime
import numpy as np
import functools


def read_topandas(file, lower=False, nanval=False):
    try:
        df = pd.read_csv(file, dtype='object')
        if lower:
            df.rename(columns=str.lower, inplace=True)
        if nanval:
            df.fillna(0,inplace=True)
        return df.to_dict(orient='records'),df
    except IOError as e:
        print(e)
    return {}, None


filename = 'WaterQualityData-MATRIZ_REDESECUND_SUB.csv'

_ , df = read_topandas('./'+filename, lower=True)

t = pd.to_datetime(df['date'], format='%Y-%m-%d', errors='coerce')


df['date'] = t.dt.strftime('%d/%m/%Y')
df.to_csv(filename,index=False)


