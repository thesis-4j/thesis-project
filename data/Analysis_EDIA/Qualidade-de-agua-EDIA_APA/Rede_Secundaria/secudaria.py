import sys
import pandas as pd
import glob
from pyproj import Transformer
converter = Transformer.from_crs('epsg:20790','epsg:4326')

file_rede='rede_Qualidadeaguassubterraneas.csv'
filename='Matriz_Rede_Secundaria_Sub.xlsx'
stations = pd.read_csv(file_rede, encoding='utf-8')
data =pd.read_excel(filename,engine='openpyxl')
data.drop_duplicates(subset=['SNIRH'], inplace=True)
stations['CODIGO']=stations['CODIGO'].astype('str')
print(len(data['SNIRH'].unique()))

unique_stations=stations[stations['CODIGO'].isin(data['SNIRH'].unique())]
lat=unique_stations.pop('COORD_X (M)')
lng=unique_stations.pop('COORD_Y (M)')
unique_stations.rename({'CODIGO':'snirh'},axis=1,inplace=True)
unique_stations.insert(0,'id',"")
unique_stations.insert(2,'latitude',lat)
unique_stations.insert(3,'longitude',lng)
data=unique_stations.fillna('')
def id(row):
    return 'WQS_RSECSNIRHSUB_'+ str(row.name)
def rule(s,row):
    lat, lon = converter.transform(row['latitude'],row['longitude'])
    if(s==0):
        return lat
    else:
        return lon

data['id']=data.apply(lambda x: id(x), axis=1)
data['latitude']=data.apply(lambda x: rule(0,x), axis=1)
data['longitude']=data.apply(lambda x: rule(1,x), axis=1)
print(data)
data.to_csv('WaterQualityStations-MATRIZREDESECSUB.csv', index=False)
