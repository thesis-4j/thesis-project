
import pandas as pd
import time
import resource
import datetime
import numpy as np
import functools

def read_topandas(file, lower=False, nanval=False):
    try:
        df = pd.read_csv(file, dtype='object')
        if lower:
            df.rename(columns=str.lower, inplace=True)
        if nanval:
            df.fillna(0,inplace=True)
        return df.to_dict(orient='records'),df
    except IOError as e:
        print(e)
    return {}, None

filename_inSitu ='data-Primaria_inSitu.csv'
filename_Matriz = 'data-Primaria_Matriz.csv'




Situ = pd.read_excel('../Resultados_Rede_Primaria.xlsx' ,sheet_name='InSitu')
print(Situ.head())
print(Situ['Campanha'])


t = pd.to_datetime(Situ['Campanha'], format='%Y-%m-%d', errors='coerce')
t = t.dt.strftime('%d/%m/%Y')
print(t.head())

_ , dfSitu = read_topandas('./'+filename_inSitu, lower=True)

dfSitu['date'] = t
print(dfSitu['date'])

dfSitu.to_csv(filename_inSitu,index=False)


#######

print("\n\nMatriz parsing")

Matriz = pd.read_excel('../Resultados_Rede_Primaria.xlsx' ,sheet_name='Matriz')
print(Matriz.head())

t2 = pd.to_datetime(Matriz['Campanha'], format='%Y-%m-%d', errors='coerce')
t2 = t2.dt.strftime('%d/%m/%Y')

_ , dfMatriz = read_topandas('./'+filename_Matriz, lower=True)

dfMatriz['date'] = t2
dfMatriz.to_csv(filename_Matriz,index=False)










