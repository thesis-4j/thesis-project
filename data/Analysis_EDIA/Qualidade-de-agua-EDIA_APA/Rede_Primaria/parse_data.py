import sys
import pandas as pd
import matplotlib.pyplot as plt

"""
file_name ="Resultados_Rede_Primaria.xlsx"
sheet="Matriz"
df = pd.read_excel(file_name, sheet_name=sheet,  engine='openpyxl')
print(df.head(), "\n")
print(df.columns, "\n")

"""

PATH = '/home/bmalbusca/Documents/IST/thesis-4j/Data/Analysis_EDIA/Qualidade-de-agua-EDIA_APA/Rede_Primaria/Resultados_Rede_Primaria_Matriz.csv'
PATH2 ='/home/bmalbusca/Documents/IST/thesis-4j/Data/Analysis_EDIA/Qualidade-de-agua-EDIA_APA/Rede_Primaria/Resultados_Rede_Primaria_InSitu.csv'
PATH3 = '/home/bmalbusca/Documents/IST/thesis-4j/thesis-project/data/Analysis_EDIA/Qualidade-de-agua-EDIA_APA/Rede_Secundaria/Matriz_Rede_Secundaria_Sup_SIG.csv'


df = pd.read_csv(PATH, dtype='object')
head1 = df.columns.values

##################
#   COMMON HEADERS
##################
df2 = pd.read_csv(PATH2, dtype='object')
head2= df2.columns.values


df3 = pd.read_csv(PATH, dtype='object')
df3 = df3.iloc[1:]
head3 = df3.columns.values

diff = [x for x in head1 if x not in head2 ] #things in head1 that are not in head2
diff2 = [x for x in head2 if x not in head1 ] #things in head2 that are not in head1
common = [x for x in head2 if x in head1]
common2 = [x for x in head3 if x in common]


#print( "attribute: ", len(common) , "\n","attribute: ", len(common2) ,"\n\n", common2,"\n\n", common)
#print( "commons", common,"\n\n")
#print( "specific to Matriz", diff ,"\n\n")
#print( "specific to InSitu", diff2, "\n")
#print( "comnon: ", len(common), " Matriz: ", len(head1), " InSitu: ", len(head2) )


##################
#   DATA
##################
df_data= df3.copy()

# Station header with exceptions SNIRH and EDIA codigo
drop_columns =  ['Infraestrutura associada', 'Sistema EFMA', 'Estação', 'Nome SNIRH (APA)', 'Latitude', 'Longitude', 'Código Massa de Água (DQA)', 'Nome Massa de Água (DQA)', 'Tipologia Massa de Água (DQA)', 'Agrupamento Massa de Água (DQA)', 'ARH', 'Programa', 'Designação EDIA (SAP)', 'Classe EDIA (SAP)', 'Entidade Preponente']
df_data_filtered= df_data.drop(columns=drop_columns, axis=1)
# verification
#print(df_data_filtered.columns.values)
#print(df_data_filtered)

#df_data_filtered.rename({'Código SNIRH (APA)':'SNIRH','Código EDIA (SAP)':'EDIA'}, axis=1,inplace=True)
#df_data_filtered.to_csv('data-Secundaria_Sup_SIG.csv', index=False)






##################
#   STATIONS
##################
station_columns = ['Infraestrutura associada', 'Sistema EFMA', 'Estação','Código SNIRH (APA)', 'Nome SNIRH (APA)', 'Latitude', 'Longitude', 'Código Massa de Água (DQA)', 'Nome Massa de Água (DQA)', 'Tipologia Massa de Água (DQA)', 'Agrupamento Massa de Água (DQA)', 'ARH', 'Programa', 'Código EDIA (SAP)', 'Designação EDIA (SAP)', 'Classe EDIA (SAP)', 'Entidade Preponente']


Matriz = df3.loc[:,station_columns].copy()
Matriz['Código SNIRH (APA)'] = Matriz['Código SNIRH (APA)'].astype('str')
Matriz['Código SNIRH (APA)'] = Matriz['Código SNIRH (APA)'].apply(lambda x: x if len(x)==6 else x[:-1])

# verification
#Matriz['len']= Matriz['Código SNIRH (APA)'].apply(lambda x: len(x))
#print(Matriz.loc[:,[ 'Código SNIRH (APA)','len']])
#print(Matriz.groupby('Código SNIRH (APA)').agg('sum'))



stations = Matriz.drop_duplicates(subset=['Código SNIRH (APA)'])
stations['Código SNIRH (APA)']=stations['Código SNIRH (APA)'].unique()
#print((stations.groupby('Código SNIRH (APA)')).head())

station_data = stations.loc[:,station_columns].copy()
station_data.rename({'Código SNIRH (APA)':'SNIRH','Código EDIA (SAP)':'EDIA'}, axis=1,inplace=True)
station_data.to_csv('stations-Secundaria_Sup_SIG.csv',index=False)



