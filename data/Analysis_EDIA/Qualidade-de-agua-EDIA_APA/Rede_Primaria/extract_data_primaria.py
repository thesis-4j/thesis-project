import sys
import pandas as pd
import datetime

filename='data-Primaria_Matriz.csv'

df = pd.read_csv(filename)


filter = df
filter.rename({'SNIRH':'snirh', 'Data recolha':'date'},axis=1, inplace=True)
cod=filter.pop('snirh')
filter.insert(0,'snirh',cod)
filter.fillna("")
print(filter)
filter.to_csv(filename,index=False)
