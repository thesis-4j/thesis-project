import pandas as pd

matriz = pd.read_csv("stations-Primaria_Matriz.csv", encoding='utf-8' )
insitu = pd.read_csv("stations-Primaria_InSitu.csv", encoding='utf-8' )
lst = []
lst.append(matriz)
lst.append(insitu)

data=pd.concat(lst, axis=0, ignore_index=True)
unique_station=data.drop_duplicates(subset=['SNIRH'], inplace=False)
#print(data)
lat=unique_station.pop('Latitude')
lng=unique_station.pop('Longitude')
code=unique_station.pop('SNIRH')

unique_station.insert(0,'id',"")
unique_station.insert(1,'snirh',code)
unique_station.insert(2,'latitude',lat)
unique_station.insert(3,'longitude',lng)
#print(unique_station)
final=unique_station.fillna('')
def id(row):
    return 'WQS_RPRISNIRH_'+ str(row.name)
final['id']=final.apply(lambda x: id(x), axis=1)
print(final)
final.to_csv('WaterQualityStations-MATRIZREDEPRISNIRH.csv', index=False)
