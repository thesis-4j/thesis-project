import pandas as pd
import glob

path='/home/bmalbusca/Documents/IST/thesis-4j/thesis-project/data/Analysis_EDIA/WQS'
all_files = glob.glob(path + "/*.csv")
lst=[]
for filename in all_files:
    df = pd.read_csv(filename, encoding='utf-8')
    lst.append(df)

data=pd.concat(lst, axis=0, ignore_index=True)
filter = data.iloc[:,0:4].copy()
print(filter)
filter.drop_duplicates(subset=['snirh'], inplace=True)
print(filter["id"].is_unique)
print(filter.shape, len(filter["id"].unique()))

ersh = pd.read_csv("WaterQualityStations-INAG1V2.csv", encoding='utf-8')
result=filter.concat(ersh.iloc[:,0:4],axis=0, ignore_index=True)
result.drop_duplicates(subset=['id'],inplace=True)
print(result)
#filter.to_csv('WaterQualityStations_9MAR21.csv', index=False)
