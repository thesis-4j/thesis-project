
import pandas as pd
import time
import resource
import datetime
import numpy as np
import functools

def read_topandas(file, lower=False, nanval=False):
    try:
        df = pd.read_csv(file, dtype='object')
        if lower:
            df.rename(columns=str.lower, inplace=True)
        if nanval:
            df.fillna(0,inplace=True)
        return df.to_dict(orient='records'),df
    except IOError as e:
        print(e)
    return {}, None


def pandas_ruler_FloatRule(row):
    try:
        float(row)
        return True
    except:
        return False

def pandas_ruler_haveNull(df):
    return df.isnull().values.any()

def pandas_ruler_isNumCol(df):
    test = pd.DataFrame()
    test['float']=df.apply(lambda x: pandas_ruler_FloatRule(x))

    for i in range(df.shape[0]):
    	if test['float'][i] == False:
        	return False

    return True


#df= pd.read_csv('./alqueva-waternodes.csv', dtype='object')
dictObj, df = read_topandas('./alqueva-waternodes2.csv', lower=True)
print("df shape:",df.shape)

##### HAVE NULL VALUES #####

#print(df.isnull().values.any())
#print("original:", df.loc[:,['id', 'name','type', 'watersystem', 'latitude','longitude']].isnull().values.any())


start_time = time.time()
print("func verify:",pandas_ruler_haveNull(df.loc[:,['id', 'name','type', 'watersystem', 'latitude','longitude']]))
print(" pandas_ruler_haveNull --- %s seconds ---" % (time.time() - start_time))


start_time = time.time()
b = df[df['longitude'].notna() & df['latitude'].notna()]
print(" df[df['longitude'].notna() --- %s seconds ---" % (time.time() - start_time))
#print(b)


print("faulty index:",list(df.index.difference(b.index))) # to return the missing indexes


start_time = time.time()
d = df[ pd.to_numeric(df[ 'latitude'], errors='coerce').notnull() & pd.to_numeric(df[ 'longitude'], errors='coerce').notnull()]
print(" pd.to_numeric --- %s seconds ---" % (time.time() - start_time))
#print(d)


######## IS A FLOAT #######
'''
def rule(row):
	try:
		float(row)
		return True
	except:
		return False

#print(df)

test = pd.DataFrame()
test['longitude']= df['longitude']
test['float']=df['longitude'].apply(lambda x: rule(x))

for i in range(df.shape[0]):
	if test['float'][i] is False:
		print("found:",i, test['longitude'])
		break
print("Rule Float values",test['float'].unique())
'''
start_time = time.time()
print("func verison:",pandas_ruler_isNumCol(df['longitude']))
print(" lng pandas_ruler_isNumCol --- %s seconds ---" % (time.time() - start_time))

start_time = time.time()
if not (pandas_ruler_isNumCol(df['longitude']) and pandas_ruler_isNumCol(df['latitude']) ):
    print("*ERROR* Invalid coordinate values")
    #flash('Invalid coordinate values')
print(" lat&lng pandas_ruler_isNumCol --- %s seconds ---" % (time.time() - start_time))



######## NULL COL ########

'''

start_time = time.time()
if pandas_ruler_haveNull(df.loc[:,['id', 'name','type', 'watersystem', 'latitude','longitude']]) :
   print("*ERROR* Missing values at ['id', 'name','type', 'watersystem', 'latitude','longitude']")
    #flash("Missing Missing values at ['id', 'name','type', 'watersystem', 'latitude','longitude']")
print("pandas_ruler_haveNull --- %s seconds ---" % (time.time() - start_time))

'''



dictObj, df2 = read_topandas('./WaterQualityData-MATRIZ_REDESECUND_SUB.csv', lower=True)



###### DATE ########

def pandas_ruler_DateRule(row):
    try:
       	d,m,y = row.split('/')
       	datetime.datetime(int(y),int(m),int(d))
       	return True
    except:
    	return False
    pass



def pandas_ruler_validDate(df):
    test = pd.DataFrame()
    test['bool']=df.apply(lambda x: pandas_ruler_DateRule(x))

    for i in range(df.shape[0]):
    	if test['bool'][i] == False:
    		return False
    return True



#print(pandas_ruler_validDate(df['date']))
start_time = time.time()
if not pandas_ruler_validDate(df2['date']):
    pass
    #print("*ERROR* Invalid date values")
    #flash('Invalid date values')
print(" pandas_ruler_validDate--- %s seconds ---" % (time.time() - start_time))

start_time = time.time()
f = df2[pd.to_datetime(df2['date'], format='%Y-%m-%d', errors='coerce').notna()]
print(" pd.to_datetime--- %s seconds ---" % (time.time() - start_time))
print(f)


####### TUTORIAL ref #######


"""

def conjunction(*conditions):
    return functools.reduce(np.logical_and, conditions)

c_1 = data.col1 == True
c_2 = data.col2 < 64
c_3 = data.col3 != 4

data_filtered = data[conjunction(c1,c2,c3)]


def disjunction(*conditions):
    return functools.reduce(np.logical_or, conditions)

c_1 = data.col1 == True
c_2 = data.col2 < 64
c_3 = data.col3 != 4

data_filtered = data[disjunction(c1,c2,c3)]



# df[f_2 & f_3 & f_4 & f_5 ] with f_2 = df["a"] >= 0  etc > df[(df['col1'] >= 1) & (df['col1'] <=1 )]
# df_original.index.difference(df_cleaned.index) to get indexes

"""
