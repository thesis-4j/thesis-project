import time
import resource
import datetime
from verify import validCSV



### TEST verify.py ValidCV

a = validCSV(validator={'header': [
                             'start', 'end', 'type']})


#test = ['start', 'end', 'type']
#header = ["start","end","type","subtype","flow_rate"]
#print(a.compare_unordered_str(test, header))


inputfile =  open('links-allsystemnodes.csv', 'r')
print("simple:",a.simple_verify_header(inputfile, decode=False))
print("complex:", a.verify_header(inputfile,decode=False))







""""
a = ['id', 'name','type', 'watersystem', 'latitude','longitude']
b = ['id','name','type','subtype','watersystem','latitude','longitude','height','power','fwl','tag']

time_start = time.perf_counter()

def test1(a,b):
	for v in a:
		if v not in b:
			return False

	return True;


def test2(a,b):
	return all(x in a for x in b)

def test3( str1: list, str2: list):
        lstr2 =  [x.lower() for x in str2]
        for v in str1:
            if v.lower() not in lstr2:
                return False

        return True

time_elapsed = (time.perf_counter() - time_start)
print("test1",test1(a,b))
memMb=resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024.0/1024.0
print ("%5.20f secs %5.10f MByte" % (time_elapsed,memMb))


time_elapsed = (time.perf_counter() - time_start)
print("test2",test2(a,b))
memMb=resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024.0/1024.0
print ("%5.20f secs %5.10f MByte" % (time_elapsed,memMb))


time_elapsed = (time.perf_counter() - time_start)
print("test3",test3(a,b))
memMb=resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024.0/1024.0
print ("%5.20f secs %5.10f MByte" % (time_elapsed,memMb))

"""