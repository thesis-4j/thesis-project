1. Rede Primária (Resultados_Rede_Primaria.xlsx), existem duas spreadsheets:  InSitu e Matriz.

   Dados In Situ são referentes a medições efectuadas no campo com uma sonda multiparamétrica. No caso das linhas de água são medições à superfície e nas albufeiras são efectuados perfis ao longo de toda a coluna de água.

   A folha designada por "Matriz" inclui os dados disponibilizados pelo laboratório.

2. Na pasta Rede Secundaria há dois ficheiros excel:

   2.1. Matriz_Rede_Secundaria_Sup.xlsx- 

   A matriz Matriz_Rede_Secundaria_Sup é referente aos resultados em linhas de água, e este programa tem por objetivo avaliar os potenciais impactes do regadio. Como no programa da rede secundária não são efectuados perfis os resultados In Situ (temperatura, pH, condutividade, oxigénio dissolvido, potencial redox e turvação) estão na mesma matriz. Todos os restantes são resultados provenientes do laboratório. As cores da matriz foram utilizadas para separar campanhas ou grupos de parâmetros e os resultados a vermelho são concentrações elevadas. Podem anular toda a formatação.

   As localização dos locais podem ser obtidos através da shapefile Estacoes_Superficiais

   2.2. Matriz_Rede_Secundaria_Sub.xlsx- São análises em laboratório de amostras colhidas em furos/poços. Não têm coordenadas mas têm o Cod SNIRH e dá para lá chegar pelo campo cod_inag que está na shapefiles de estações subterrâneas.

    Também neste caso os parâmetros temperatura, pH, condutividade, oxigénio dissolvido, potencial redox e turvação são determinados no campo com recurso a uma sonda multiparamétrica, sendo os restantes provenientes do laboratório. A informação sobre se são furos ou poços está na shapefile.

   

3. Os dados na pasta Dados ERHSA são mais antigos.(1996-1999) São todos furos/poços.

   3.1. No ficheiro sig_ERHSA_quimica.xls, há duas spredsheets, Aguas altas e Aguas baixas. O que as diferencia é o período de amostragem, Águas Altas (Março-Maio); Águas Baixas (Setembro-Novembro). Não têm coordenadas mas têm códigos N_ERHSA e N_Ordem, respectivamente. Ambos têm a mesma configuração, como por exemplo 501G052, têm informação relativamente ao nº da carta militar (501), uma Letra (G) que representa  a Designação da equipa de projeto- G-IGM; U-Universidade de Évora; A-INAG; R-DRAOT e o número da estação (052) (Sequência N+1 na Carta Militar) .

   O ficheiro PontosINAG.xlsx tem coordenadas e o N_ERHSA que tem correspondência com os campos N_ERHSA e N_Ordem do ficheiro com dados das análises químicas.  

   3.2. Na pasta Dados SNIRH a informação que está nos ficheiros excel e na geodatabase (ficheiros sig) é a mesma. Os ficheiros excel deverão ter sido extraídos dos ficheiros sig e não têm informação nova. Trata-se de dados estáticos desde 1999. Foram oriundos de várias entidades, qualquer dúvida sobre os dados em si não há condições de esclarecer, se não servir será para abandonar.


