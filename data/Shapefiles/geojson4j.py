
import os
import glob
import geojson_utils as geojson
import json
import geopandas
import uuid
from datetime import date
import pandas as pd

from flask import Flask, flash, abort, request, make_response
from flask_caching import Cache
from flask_cors import CORS
from neo4j import GraphDatabase as GDB, basic_auth, exceptions


# Database session
IP = os.getenv('IP_NEO4J', "localhost")
PORT = os.getenv('PORT_NEO4J', "7687")
ADDR = "bolt://" + IP + ":" + PORT
USR = os.getenv('USR_NEO4J', "neo4j")
PWD = os.getenv('PWD_NEO4J', "malbusca1995")
TIMEOUT = 100.0

try:
    print("Connecting driver ...")
    drv = GDB.driver(ADDR, auth=basic_auth(USR, PWD), connection_timeout=TIMEOUT)  # driver's database connection
    ses = drv.session()  # session opening
    print("Connection established.")
except exceptions.ServiceUnavailable as exc:
    ses = None
    print("WARNING:", exc)




QUERY = """ CALL apoc.load.json("file:///Bacias_hidrograficas_wgs84.geojson")
            YIELD value as json
            MERGE (shp:Shapefile{filename: "Bacias_hidrograficas_wgs84.geojson" , type: json.type, description: "bacias"})
            ON CREATE SET shp.created = datetime()
            WITH shp, json
            UNWIND json.features as features
            MERGE (f:Feature {Name: features.properties.name, type: features.type})
            SET f += features.properties
            WITH shp, f, features
            CALL apoc.convert.setJsonProperty(f, 'geojson',features)
            CREATE (f)-[:FEATURE]->(shp)
            RETURN f.name, shp.filename

"""



nodejson = geopandas.read_file('./Shapefiles_EDIA/Albufeiras_Reservatorios_wgs84.shp').to_json()



#resp = ses.run(queries.QUERY).data()
#print(resp)
