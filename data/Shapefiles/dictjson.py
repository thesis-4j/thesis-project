import json

file = open('Bacias_hidrograficas_wgs84.geojson', 'r')
geodata = json.load(file)

print("before:",geodata['features'][0]["properties"])

for f in geodata['features']:
    for key in f["properties"].keys():
        new = {}
        # f["properties"][key.lower()]= f["properties"].pop(key)
        new[key.lower()] = f["properties"][key]
        f["properties"] = new

print("after:",geodata['features'][0]["properties"])
