import sys
#import pandas as pd
#import matplotlib.pyplot as plt

#import shapefile #pyshp
import os
import glob
import geojson_utils as geojson
import json
import geopandas
import uuid
from datetime import date
import pandas as pd
#from shapely.geometry import Point, Polygon

## for future https://www.geodose.com/2020/06/pyqgis-tutorial-shapefile-conversion.html


""" read xlsx
file_name ="Resultados_Rede_Primaria.xlsx"
sheet="Matriz"
df = pd.read_excel(file_name, sheet_name=sheet,  engine='openpyxl')
print(df.head(), "\n")
print(df.columns, "\n")
"""
"""
shape = shapefile.Reader("./Shapefiles/Shapefiles_EDIA/Albufeiras_Reservatorios_wgs84.shp")
#first feature of the shapefile
feature = shape.shapeRecords()

#print(feature)
data = feature[0].shape.__geo_interface__
p1 = Point(24.952242, 60.1696017)
print(data) # (GeoJSON format)
print("shape contains p1?", p1.within(Polygon(data["coordinates"][0])))
box_str_test = '{"type": "Polygon","coordinates": [[[0, 0],[10, 0],[10, 10],[0, 10]]]}'
shape_box = json.loads(box_str_test)

#box_str=str(data).replace("(","[").replace(")", "]")
#s= str(box_str).replace("\'","\"")
#box= "'"+s+"'"

#test= str(data)
#shape_box = json.loads(test)
centroid = geojson.centroid(shape_box)

print(centroid['coordinates'])
"""

"""
geopandas
"""


def save_json():
    pass
'''
def read_shpfiles(dir_path, filetype):
    for dir_filename in glob.glob(os.path.join(dir_path, filetype)):
        try:
            with open(os.path.join(os.getcwd(), dir_filename), 'r') as f:
                filename = (dir_filename.rsplit("/",1)[1])

        except e:
            print(e)
'''

# path = '../Shapefiles/Shapefiles_EDIA/'
path = './Shapefiles_EDIA/UsoSolo2018'
dpath = './cache/'
filetype = '*.shp'

def shp_to_json(path, dpath='./cache/', filetype = '*.shp' ):
    for dir_filename in glob.glob(os.path.join(path, filetype)):
        with open(os.path.join(os.getcwd(), dir_filename), 'r') as f:
           filename = (dir_filename.rsplit("/",1)[1]).split(".",1)[0]
           with open (dpath+filename+'.json', 'w') as jsonfile:
               json.dump(geopandas.read_file(dir_filename).to_json(), jsonfile )

def load_shp(path, dpath='./cache/', filetype = '*.shp' ):
    shpfiles={} #optimizar isto
    for dir_filename in glob.glob(os.path.join(path, filetype)):
        try:
            geodata = geopandas.read_file(dir_filename)
            print(geodata)
            obj = {'name':(dir_filename.rsplit("/",1)[1]).split(".",1)[0] ,'date':date.today().strftime("%d/%m/%Y"), 'features': geodata , 'datatype': 'GeoJSON' }
            shpfiles[obj['name']]=obj
        except Exception as e:
            print(e, dir_filename)
    return shpfiles


def json_obj(path, dpath='./storage/', filetype = '*.shp'):
    for dir_filename in glob.glob(os.path.join(path, filetype)):
        shpfile = geopandas.read_file(dir_filename)
        ID = uuid.uuid4()
        shpfile.to_file( dpath + str(ID) +'.geojson', driver='GeoJSON')
        print(dir_filename.rsplit("/",1)[1], ID)

#if __name__ == "__main__":
#    print(load_shp(path))


myshpfile = geopandas.read_file("./Shapefiles_EDIA/Bacias_hidrograficas_wgs84.shp")
print(myshpfile.centroid)
try:
    myshpfile.to_file('./Bacias_hidrograficas_wgs84.geojson', driver='GeoJSON')
except Exception as e:
    print(e)

#json_obj(path)
#load_shp(path)
#soil= geopandas.read_file('./UsoSolo2018/Bacias_hidrograficas_UsoSolo2018_wgs84.shp')
#soil= geopandas.read_file('./Shapefiles_EDIA/Bacias_hidrograficas_wgs84.shp')



#print(soil.to_dict('records')[0], soil.columns)

#df=soil.copy()
#print(df.to_json())
#df['json'] = df.apply(lambda x: x.to_json(), axis=1)
#geolist= []
#for index, row in df.iterrows():
#    geolist.append(row.to_json())
#print(geolist)

#geojson2 = {"type": "Feature", "geometry": { "type": "Point","coordinates": [125.6, 10.1]},"properties": {"name": "Dinagat Islands"}}
#test = geopandas.GeoSeries(geojson2)
#print(test)

#test2 = geopandas.GeoDataFrame([geojson2])
#print(test2)

#test3 = pd.DataFrame([geojson2])
#print(test3)
