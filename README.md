


#  <img src="./assets/dam_120739.png" width="50"/> ediawater



## Getting Started



###	Architecture

![Ediawater](./docs/images/Web_App_architecture.png)



### Prerequisites

-   Suported OS
    -   MacOS
    -   Linux

- Pre-installation requirements
    -   Python3+
    -   NodeJS
    -   npm

### Installation

How to install Ediawater ? 

- Database (Neo4j) deployment : https://gitlab.com/thesis-4j/thesis-project/-/blob/master/README.md
  - The steps are listed below this section.
- API server (Flask) installation and deployment : https://gitlab.com/thesis-4j/backend-ediawater/-/blob/86874602949c82d0cdfdda301c541e70bd76b999/README.md
- UI Web App (ReactJS Frontend) installation and deployment :https://gitlab.com/thesis-4j/frontend-ediawater/-/blob/44d05a33936d97782d8b9434535828d57c6bcdee/README.md

## Deployment

### Neo4j Database

#### Google Cloud Platform

##### Create a new GC Instance

1.   Create a New Project

2.  Go to `Compute Engine` (left side options)

3.  Select `VM instances`

    1.   Select `Create Instances` (top bar)
    2.  Choose your region
    3.   Choose your Machine configuration
    4.  Choose OS at Boot disk section: `Ubuntu 20.04` or `Debian 10 buster`
    5.  Allow HTTP and HTTPS traffics
    6.  Just click Create

4.  [Add new firewall rules](https://cloud.google.com/vpc/docs/using-firewalls) at VPC network

    1.  Click on `create firewall rule` 
    2.  Add name: `allowbolt`
    3.  Selection **ingress** direction
    4.  Select **Allow** action
    5.  Choose targets **All instances in the network**
    6.  Add source ip: `0.0.0.0/0` 
    7.  Select **allow all** on Protocols and ports

    


#### Debian 10 VM

-   [Follow official guide](https://neo4j.com/docs/operations-manual/current/installation/linux/debian/#debian-installation)

#### Ubuntu 20.04 VM 

##### Connect to  VM instance via SSH

###### Install Java

-   [Install a compatible java version]() with neo4j

    1.  Add jdk repository `sudo add-apt-repository -y ppa:openjdk-r/ppa`
    2.  `sudo apt-get update`
    3.  `java --version`

    If the version is not suitable, check which version is available at apt

    1.  `apt-cache search 'openjdk'`

    2.  Choose the version from `apt-cache`, ex ` openjdk-11-jdk`:   `sudo apt install openjdk-11-jdk`

        -   In this case, you should update `JAVA_HOME`

            ```bash
            sudo update-alternatives --list java #check avaialble java path
            sudo update-alternatives --config java #select the default java
            export JAVA_HOME=<java-path-from-alternatives-list> #/usr/lib/jvm/java-11-openjdk-amd64
            export PATH=$JAVA_HOME/bin:$PATH
            source ~/.bashrc
            ```

            

###### [Install Neo4j Server](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-neo4j-on-ubuntu-20-04) [(official)](https://neo4j.com/docs/operations-manual/current/installation/linux/debian/#debian-installation)

-   Install prerequisites packages

    	1. `sudo apt install apt-transport-https ca-certificates curl software-properties-common ufw net-tools`
    	2. `sudo apt-get update`

-   Add Neo4j repository

    -    Latest version 

        ```bash
        wget -O - https://debian.neo4j.com/neotechnology.gpg.key | sudo apt-key add -
        echo 'deb https://debian.neo4j.com stable latest' | sudo tee /etc/apt/sources.list.d/neo4j.list
        sudo apt-get update
        ```

    -   [Specific version](https://debian.neo4j.com/), ex `4.1.x`

        ```bash
        curl -fsSL https://debian.neo4j.com/neotechnology.gpg.key | sudo apt-key add -
        sudo add-apt-repository "deb https://debian.neo4j.com stable 4.1"
        ```

-   Install Neo4j

    -   [Check for available packages](https://neo4j.com/developer/kb/using-apt-get-to-download-a-specific-neo4j-debian-package/): `apt-cache madison neo4j`

    -   Install specific version: `apt-get install neo4j=1:4.1.3`

    -   try to run:

        ```bash
        sudo systemctl enable neo4j.service
        sudo systemctl restart neo4j.service #the service takes some time to restart, maybe will fail first. try more 1x
        sudo systemctl status  neo4j.service
        ```

        -   If successful, copy 

-   Config Neo4j

    -   Git clone [ediawater's repo](https://gitlab.com/thesis-4j/thesis-project) : `git clone https://gitlab.com/thesis-4j/thesis-project.git`

    -   Add your `neo4j.conf` and `apoc.conf ` to `/etc/neo4j/`

        -   `cp database/conf/neo4j.conf /etc/neo4j/`
        -   Give file permissions: `chmod 757 apoc.conf neo4j.conf`

    -   Add compatible `apoc-procedures` to `/var/lib/neo4j/plugins`

        -   Go to [Github neo4j-contrib/neo4j-apoc-procedures repo](https://github.com/neo4j-contrib/neo4j-apoc-procedures/releases/) and download the compatible version:

            ```bash
            curl -LJO https://github.com/neo4j-contrib/neo4j-apoc-procedures/releases/download/4.1.0.6/apoc-4.1.0.6-all.jar
            
            # or with wget 
            
            wget https://github.com/neo4j-contrib/neo4j-apoc-procedures/releases/download/4.1.0.6/apoc-4.1.0.6-all.jar
            ```

        -   Check if jar file is not corrupt: `java -jar apoc-*.jar`

        -   Give permissions: `chmod 757 apoc-*.jar`

    -   Try to run. If failed, try to copy instead the apoc version at `etc/var/liv/neo4j/labs` 

-   Open firewall port

    ```bash
    sudo ufw status
    sudo ufw allow 7687
    sudo ufw allow ssh
    sudo ufw enable
    sudo ufw reload
    sudo ufw status
    ```

-   Start Neo4j service

    ```bash
    ulimit -n
    ulimit -n 40000
    
    sudo systemctl enable neo4j.service
    sudo systemctl restart neo4j.service
    sudo systemctl status  neo4j.service
    ```

    -    Check if  Neo4j service is accessible from outside : `netstat -ptlen`
    -   Use `journalctl -u neo4j` if neo4j.service failed

-   Change Neo4j admin password:

    ```bash
    hostname -I	#get local ip
    cypher-shell -a 'neo4j://<localip>:7687'
    ```

######  Install Neo4j desktop

-   Check the package name `neo4j-community-x.x.x-unix.tar.gz` at downloads page and then download: `wget http://dist.neo4j.org/neo4j-community-4.1.8-unix.tar.gz`
-   Config Neo4j
-   Open firewall port
-   Start Neo4j: `neo4j-community-xx/bin/neo4j start`
-   Change Neo4j admin password: `cypher-shell`

## Contributing



## Versioning



## License

See the [LICENSE](LICENSE) file for more details.

## Acknowledgments

-  [mapcoordinates.net](https://www.mapcoordinates.net/en)
-  [react-grid-layout](https://github.com/strml/react-grid-layout)
-  [PATKENNEDY79](https://www.patricksoftwareblog.com/setting-up-gitlab-ci-for-a-python-application/)
-  [po5i](https://dev.to/po5i/how-to-add-basic-unit-test-to-a-python-flask-app-using-pytest-1m7a)
-  [aaronjolson](https://github.com/aaronjolson/flask-pytest-example)
-  [nvie](https://nvie.com/posts/a-successful-git-branching-model/)



