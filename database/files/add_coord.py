import pandas as pd

coord = pd.read_csv("coord2.csv", encoding='utf-8').drop(columns= ['alias'])
print("coord",coord.columns)

FILENAME = 'pedrogao-waternodes.csv'
data = pd.read_csv(FILENAME, encoding='utf-8')

print("data",data.columns)
complete = data.merge(coord, on=['type', 'name'])
lat=complete.pop('latitude_y')
lng=complete.pop('longitude_y')
h=complete.pop('height_y')
complete['latitude_x']=lat
complete['longitude_x']=lng
complete['height_x']=h

complete.rename({'latitude_x':'latitude', 'longitude_x':'longitude', 'height_x':'height'},axis=1,inplace=True)
print(complete.columns)
print(complete)
complete.fillna('')
complete.to_csv(FILENAME, index=False)
