import pandas as pd


df = pd.read_csv("alqueva-hydrographicnodes.csv", encoding='utf-8' )

def id(row):
    return 'WN_ALQUEVA_'+row['type'][:1].upper()+str(row.name)

df['id']=df.apply(lambda x: id(x), axis=1)
print(df)
df.to_csv('alqueva-hydrographicnodes.csv', index=False)
