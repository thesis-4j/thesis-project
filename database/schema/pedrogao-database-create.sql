//-------------------------------------------------------------------------------
//-- Name 			EDIA Graph Database
//-- Version		0.2v
//-- Author       	Bruno Figueiredo
//-- Created      	17/10/2020
//-- Purpose      	Modeling the Pedrógão subsystem and converting it into a graph database
//--
//-- History 		Tested
//--
//-- Copyright © 2020, EDIA S.A and Instituto Superior Técnico - Ulisboa, All Rights Reserved
//-------------------------------------------------------------------------------

//-- Rules
//
// Note: For the Reservatories that have a common name (ex. R1),
//		 The naming rule for these cases is to add the name of the Hidrographic Exploration at the beginning.
//
// 		 Example: name:  '<hidrographic_name_no_space> R1'
//
// Note: Pumping stations labeling  'source-destination';  if there is multiple destinations 'source-destination1-destination2-...-destinationX'
//		 If a Pumping station is related to other node (ex: Reservatories), the relation between source and Pumping station  will be the same as the source and reservatory
//		 If it will be sourcing an end, the tag will be 'src'
//
// Note: Canals will have tag 'destination' if it will be sourcing an end,  and  'source-destination' if it is a mid-point in the hidrographic cirucit


// ----------------------------------------------------------- //
// CH Pedrógão - CH S.Matias Circuit


// Reservatories and Dams

CREATE (Almeidas:WaterNode {name:'Almeidas',type:'Barragem', subtype:'', tag:'distribution', power:0, fwl:193.3, location: Point({latitude: 38.19746677 , longitude: -7.49623775 , height:110, crs: 'wgs-84-3d'})})
CREATE (SPedro:WaterNode {name:'São Pedro',type:'Barragem', subtype:'', tag:'distribution', power:0, fwl:142.5, location: Point({latitude: 38.19746677 , longitude: -7.49623775 , height:110, crs: 'wgs-84-3d'})})
MERGE (Pedrogao:WaterNode {name:'Pedrógão',type:'Barragem', subtype:'', tag:'origin', power:10, fwl:84.8,location: Point({latitude: 38.19746677 , longitude: -7.49623775 , height:110, crs: 'wgs-84-3d'})})


CREATE (RPedrogao:WaterNode {name:'R. Pedrógão' , type:'Reservatório', subtype:'',tag:'distribution', power:0, fwl:156.0,location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})
CREATE (RSelmes:WaterNode {name:'R. Selmes' , type:'Reservatório', subtype:'',tag:'distribution', power:0, fwl:198.0,location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})
CREATE (RCegonha:WaterNode {name:'R. da Cegonha' , type:'Reservatório', subtype:'',tag:'distribution', power:0, fwl:198.0,location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})

//CREATE (RCegonha:WaterNode {name:'R. da Cegonha' , type:'Reservatório', subtype:'',tag:'distribution', power:0, fwl:198.0,location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})


// Canals/Water Conduits
//CREATE (SMatias1Fork:WaterNode {name:'S. Matias 2.1', type:'Conduta', subtype:'', tag:'distribution', power:0, fwl:0,location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})
//CREATE (SMatias2Fork:WaterNode {name:'S. Matias 2.2', type:'Conduta', subtype:'', tag:'distribution', power:0, fwl:0,location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})


// Pumping stations
CREATE (PedrogaoElev1:WaterNode {name:'Pedrógão-Pedrógão1' , type:'Estação Elevatória', subtype:'Secundária',tag:'distribution', power:2.5, fwl:0,location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})
CREATE (PedrogaoElev2:WaterNode {name:'Pedrógão-R.Pedrógão' , type:'Estação Elevatória', subtype:'Primária',tag:'distribution', power:12.0, fwl:0,location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})
CREATE (RPedrogaoElev:WaterNode {name:'R.Pedrógão-Pedrógão3' , type:'Estação Elevatória', subtype:'Secundária',tag:'distribution', power:1.9, fwl:0,location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})
CREATE (RSelmesElev:WaterNode {name:'R.Selmes-Selmes2-Selmes5' , type:'Estação Elevatória', subtype:'Secundária',tag:'distribution', power:2.5, fwl:0,location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})
CREATE (SPedroElev1:WaterNode {name:'SPedro-RCegonha' , type:'Estação Elevatória', subtype:'Primária',tag:'distribution', power:3.6, fwl:0,location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})
CREATE (SPedroElev2:WaterNode {name:'SPedro-Amendoira' , type:'Estação Elevatória', subtype:'Primária',tag:'distribution', power:8.5, fwl:0,location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})
CREATE (AlmeidasElev:WaterNode {name:'Almeidas-SMatias2-SMatias3' , type:'Estação Elevatória', subtype:'Secundária',tag:'distribution', power:3.8, fwl:0,location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})



CREATE
(Pedrogao)-[:CONNECTED{type:'Aduação', subtype:'Bombagem', flow_rate: 12.5}]->(PedrogaoElev1),
(Pedrogao)-[:CONNECTED{type:'Aduação Blocos de Rega', subtype:'Pressão', flow_rate: 0}]->(PedrogaoElev2),
(PedrogaoElev1)-[:CONNECTED{type:'Aduação', subtype:'Bombagem', flow_rate: 12.5}]->(RPedrogao),
(RPedrogao)-[:CONNECTED{type:'Aduação Blocos de Rega', subtype:'Pressão', flow_rate: 0}]->(RPedrogaoElev),
(RPedrogao)-[:CONNECTED{type:'Aduação', subtype:'Gravidade', flow_rate: 9.5}]->(SPedro),
(RPedrogao)-[:CONNECTED{type:'Aduação', subtype:'Gravidade', flow_rate: 1.5}]->(RSelmes),
(RSelmes)-[:CONNECTED{type:'Aduação Blocos de Rega', subtype:'Pressão', flow_rate: 0}]->(RSelmesElev),
(SPedro)-[:CONNECTED{type:'Aduação', subtype:'Bombagem', flow_rate: 4.5}]->(SPedroElev1),
(SPedro)-[:CONNECTED{type:'Aduação', subtype:'Bombagem', flow_rate: 4.5}]->(RCegonha),
(RCegonha)-[:CONNECTED{type:'Aduação', subtype:'Gravidade', flow_rate: 3.8}]->(Almeidas),
(Almeidas)-[:CONNECTED{type:'Aduação Blocos de Rega', subtype:'Pressão', flow_rate: 0}]->(AlmeidasElev)


// ----------------------------------------------------------- //
// CH Pedrógão-Baleizão - CH Baleizão-Quintos Circuit


// Reservatories and Dams

CREATE (Amendoeira:WaterNode {name:'Amendoeira',type:'Barragem', subtype:'', tag:'distribution', power:0, fwl:194.0,location: Point({latitude: 38.19746677 , longitude: -7.49623775 , height:110, crs: 'wgs-84-3d'})})
CREATE (Magra:WaterNode {name:'Magra',type:'Barragem', subtype:'ligação Abastecimento Público', tag:'distribution', power:0, fwl:194.0,  location: Point({latitude: 38.19746677 , longitude: -7.49623775 , height:110, crs: 'wgs-84-3d'})})
CREATE (REstacio:WaterNode {name:'R. Estácio' , type:'Reservatório', subtype:'',tag:'distribution', power:0, fwl:189.0,location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})
CREATE (RBaleizaoQuintos:WaterNode {name:'Baleizão-Quintos R1' , type:'Reservatório', subtype:'',tag:'distribution', power:0, fwl:228.5,location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})


// Canals/Water Conduits
//CREATE (SPedroBalSul2Fork:WaterNode {name:'S.Pedro Bal. Sul', type:'Conduta', subtype:'', tag:'distribution', power:0, fwl:0,location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})
//CREATE (BalQuintos1Fork:WaterNode {name:'Baleizão-Quintos 1', type:'Conduta', subtype:'', tag:'distribution', power:0, fwl:0,location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})
//CREATE (BalQuintos2Fork:WaterNode {name:'Baleizão-Quintos 2', type:'Conduta', subtype:'', tag:'distribution', power:0, fwl:0,location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})
//CREATE (BalQuintos3Fork:WaterNode {name:'Baleizão-Quintos 3', type:'Conduta', subtype:'', tag:'distribution', power:0, fwl:0,location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})


// Pumping stations
CREATE (REstacioElev:WaterNode {name:'R.Estácio-BaleizãoQuintosR1' , type:'Estação Elevatória', subtype:'Secundária',tag:'distribution', power:2.9, fwl:0, tia:0, block:'',altitude: [0,0],location:Point({latitude:0, longitude:0, height:0,crs: 'wgs-84-3d'})})


CREATE
(SPedro)-[:CONNECTED{type:'Aduação', subtype:'Bombagem', flow_rate: 0}]->(SPedroElev2),
(SPedroElev2)-[:CONNECTED{type:'Aduação', subtype:'Bombagem', flow_rate: 0}]->(Amendoeira),
(Amendoeira)-[:CONNECTED{type:'Aduação', subtype:'Gravidade', flow_rate: 0}]->(Magra),
(Magra)-[:CONNECTED{type:'Aduação', subtype:'Gravidade', flow_rate: 0}]->(REstacio),
(REstacio)-[:CONNECTED{type:'Aduação', subtype:'Bombagem', flow_rate: 0}]->(REstacioElev),
(REstacioElev)-[:CONNECTED{type:'Aduação', subtype:'Bombagem', flow_rate: 0}]->(RBaleizaoQuintos)







