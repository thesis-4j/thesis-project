> (EFMA - Empreendimento de Fins Múltiplos de Alqueva) 

The Alqueva Multi-Purpose Project (EFMA Project) is located in the Alentejo region of Portugal and consists on  water abduction and storage infrastructures, as well as infrastructures to improve irrigation areas with the intention to increase the irrigated agriculture practice. [Extended%20Abstract_AMG.pdf]  

The EFMA project is centered on the Alqueva dam, built on the Guadiana river is composed by  69 dams and reservoirs and has 380 km of primary network. Irrigates about 120 000 ha of agricultural land, ensures water for public supply to 200 000 people and produces enough hydro electricity to supply a city with more than 500 000 inhabitants.[https://www.edia.pt/en/alqueva/] 

The EFMA water system is divided into three subsystems according by water source localization: água: Alqueva, Pedrógão e Ardila.

Guarantee a water supply even in periods of extreme drought to an area of around 10,000 km2, in a total of 20 municipalities in the districts of Beja, Évora, Portalegre and Setúbal [from https://www.edia.pt/en/alqueva/the-territory/].
 the source of a system of 69 dams and reservoirs.[https://www.edia.pt/en/alqueva/] 380 km of the primary network 



The official goals for the project is : strategic water reserve; changing the
agricultural model in Alentejo; electricity generation; promotion of tourism; combat of desertification
and climate change; intervention in the conservation of natural and cultural heritage; promotion of
local employment. [/2005IAIA_AlquevaDam.pdf]



> O Sistema Global de Rega do EFMA divide-se em três subsistemas, de acordo com as diferentes origens de água: Alqueva, Pedrógão e Ardila, com base nas duas albufeiras principais Alqueva e Pedrógão.



The agricultures is the dominant activity over this region, where extensive rainfed agriculture and Agro-silvo-pastoralism [RNT1439] is considerable percentage over total type of agriculture exploration. Additionally, there is horticulture and horticultural-related industry activities.  

> Domina a actividade agricola em que o montado e as exploracoes extensivas de sequeiro determinam uma paisagem de campos aberto. A existencias de quequenas áreas de culturas horticulas e horto-industrieais. 



In some regions the detrictic soil exposes an enourmous subterranean waters' vulnerability to pollution. The soil caracteristics are prone to an quick infiltrations of water and contaminants  to the fenatic level, emerging disadvantageous conditons to depuration of contaminated waters. The chemical usage in agriculture can affect directly the water quality via  the terrain's drainage characteristics which will influenciate surfaces waters such water streams and ponds that present an enriched aquatic habitat for epilithic and interstitial aquatic species. The phosphate, nitrogen , nitrates  and organics are the main chemical substances that  associated to livestoock farming[from M00940] that promotes eutrophication.   Near the more densely polulated zones, urban waste water discharges represents other form of source of water contamination [From Estudo De Impacte Ambiental Do] with specifc cases like Serpa region that have a low water treatment index  where only 47 percent of Serpa's population have waterwaste conducted to a water treament station. 



> o caráter detrítico dos terrenos reflecte a grande vulnerabilidade à poluição a que as águas subterrâneas estão sujeitas. As características dos terrenos favorecem a rápida infiltração das aguas e dos contaminantes arrastados até ao nivel freático, conferindo-lhes condições de desfavoraveis à depuracao dos poluentes devido à grande velocidade com que se processa o fluxo em profundidade .  O escoamento superficial dos terrenos e os produtos associados à actividade agriicol afectarão directamente a qualidade das águas das linhas de água que ainda mantêm uma galeria ripícola muito desenvolvida e deversas espécies aquáticas, associadas ao meio ou favorecidads pela proxximidade às linhas de água. Em termos gerais as aguas reflectem a contaminação de origem agricola com valores superiores ao valor máximo recomendável para os fosfatos, azoto e nitratos e. organica devido essencialmente a explorações agropecuárias. [from M00940]
> descargas de águas residuais domésticas tratadas nos sistemas de tratamento dos vários aglomerados populacionais da área de análise, às águas residuais de algumas instalações de bovinicultura e suinicultura  [From Estudo De Impacte Ambiental Do]

> Os resíduos predominantes são os de origem doméstica e agrícola.Ao nível dos efluentes líquidos, o concelho de Serpa apresenta um baixo índice de tratamento, com apenas 47 percento de população servida com ETAR em funcionamento. [**From Estudo de Impacte Ambiental dos Adutores de Pedrógão, Brinches-Enxoé e Serpa**]





The EFMA project is managed by EDIA (Empresa de Desenvolvimento e Infra-estruturas do Alqueva, S.A.), the state-owned company  (SEE, Setor Empresarial do Estado) which have the mission is to boost the economy, social development and environmental preservation by working towards making the most of the territory and water resources and promoting irrigation [https://www.edia.pt/en/what-we-do/construction-of-infrastructures/].



> stated in 2005 by EDIA, the state-owned company charged with
> managing the Alqueva project, are the following (www.edia.pt): 

> As manager of the Alqueva Project, EDIA’s mission is to boost the economy, social development and environmental preservation by working towards making the most of the territory and water resources and promoting irrigation. 

> Consistent with European Union policy and priroities, and the mission of EDIA, we recommend these concerns be addressed in detailed management plans with clear accountability standards in a revised Environmental Management Programme Under both the European Water Framework Directive and the Portuguese Water Plan (*Plano Nacional da Água*),

In accordance with European Union and Portuguese water policy, EDIA has followed an environmental policy that supports sustainable development in its catchment area as well as enhancing, mitigating, and enhancing the Project's impacts. [https://www.edia.pt/en/social-responsability/environmental-policy/]

EDIA’s Environmental Management Programme (EMP 2005), which was approved by Joint Ordinance 1050/2005 of 6 December of the Ministry of the Environment, Territorial Planning and Regional Development and Ministry of Agriculture, Rural Development and Fisheries provides for the promotion and coordination of the design and implementation of environmental monitoring programmes for different aspects of the EFMA. [https://www.edia.pt/en/what-we-do/monitoring/]

The EDIA is responsible for monitoring environmental factors in the EFMA from Status of surface and underground bodies of water to Soils, Fauna and Flora [from https://www.edia.pt/en/what-we-do/monitoring/], 

> EDIA is responsible for monitoring environmental factors in the EFMA’s different phases:
> Status of surface bodies of water
> Status of underground bodies of water
> Fauna and flora
> Soils [from https://www.edia.pt/en/what-we-do/monitoring/]





and is encharged for implement and research new climatological and hydrometric monitoring stations [https://www.edia.pt/pt/o-que-fazemos/monitorizacao/agua/]



> ​	A EDIA, enquanto entidade gestora do Empreendimento de Fins Múltiplos de Alqueva (EFMA) promove a implementação de um conjunto de programas de monitorização, sendo responsável pelas seguintes ações: 
>
> - Implementação e exploração de novas estações climatológicas e hidrométricas, no âmbito da gestão do EFMA; Monitorização do estado das massas de águas superficiais e subterrâneas durante as fases de construção e exploração do Empreendimento.
>
>   [https://www.edia.pt/pt/o-que-fazemos/monitorizacao/agua/]



The EDIA's water quality monitoring programme has the goals of  



Assess the suitability of the water in transit through the system and its adaptability to the uses provided for in the Concession Contract entered into between the Portuguese State and EDIA

Ensure compliance with the laws in force and environmental commitments  , given EDIA's responsibilities

Collect data to support decision-making, with a view to the management and operation of EFMA

Assess the effectiveness of the ecological flows and other mitigation measures implemented, or to be implemented

Safeguard EDIA from responsibility for any degradation in water quality resulting from actions promoted by third parties

Assess potential impacts of water transfer between Loureiro and Alvito reservoirs. Assess the potential impacts of agricultural runoff on surface and groundwater resources

> O programa de monitorização de qualidade da água na rede primária do EFMA visa: • Recolher dados de suporte à gestão e exploração do EFMA; • Avaliar a adequabilidade da água captada para o uso rega; • Avaliar a adequabilidade do caudal ecológico libertado para jusante; • Assegurar o cumprimento dos diplomas legais em vigor, face às responsabilidades da EDIA; • Assegurar o cumprimento dos compromissos ambientais da responsabilidade [PublicacaoMassasAgua_07032018.pdf] 



> A monitorização das massas de água tem como objetivos:
>
> - Avaliar a adequabilidade da água em trânsito no sistema e a sua adaptabilidade aos usos previstos no Contrato de Concessão, celebrado entre o Estado Português e a EDIA; Integrar as disposições de monitorização resultantes dos diplomas legais em vigor, face às responsabilidades da EDIA; Recolher os dados de suporte à tomada de decisão, com vista à gestão e exploração do EFMA; Avaliar a eficácia dos caudais ecológicos e de outras medidas de mitigação implementadas, ou a implementar; Salvaguardar a EDIA da responsabilidade de uma eventual degradação da qualidade da água decorrente de ações promovidas por terceiros; Avaliar os potenciais impactes da transferência de água entre as albufeiras do Loureiro e Alvito; Avaliar os potenciais impactos das escorrências agrícolas sobre os recursos hídricos superficiais e subterrâneos
>
>   [https://www.edia.pt/pt/o-que-fazemos/monitorizacao/agua/]



> A autoridade de Avaliação de Impactes Ambientais (AIA) é a Agência Portuguesa do Ambiente (APA). 

The Portuguese Environment Agency (APA) is a public institute, within the scope of the Portuguese Environment and Climate Action Ministry [https://apambiente.pt/en/apa/portuguese-environment-agency-apa].



> A APA, como Autoridade Nacional da Água, tem competências no desenvolvimento e na implementação deste quadro legal, nomeadamente da Lei da Água, que transpõe a principal legislação Europeia relativa à água - a Diretiva Quadro da Água[https://apambiente.pt/agua/legislacao]. A **APA** tem competências de monitorização, planeamento e avaliação, licenciamento e fiscalização, sendo por isso o principal regulador ambiental em Portugal. [https://apambiente.pt/apa/quem-somos-e-o-que-fazemos]

APA, as the National Water Authority, has competencies in the development and implementation of this legal framework, namely the Water Law, which transposes the main European water legislation - the Water Framework Directive. The **APA** has monitoring, planning and evaluation, licensing and inspection powers and is therefore the main environmental regulator in Portugal.

> A APA, como Autoridade Nacional da Água, tem competências no desenvolvimento e na implementação deste quadro legal, nomeadamente da [Lei da Água](https://apambiente.pt/agua/lei-da-agua), que transpõe a principal legislação Europeia relativa à água - a [Diretiva Quadro da Água](https://eur-lex.europa.eu/resource.html?uri=cellar:5c835afb-2ec6-4577-bdf8-756d3d694eeb.0009.02/DOC_1&format=PDF). [https://apambiente.pt/agua/legislacao]

APA, as the National Water Authority, has competencies in the development and implementation of this legal framework, namely the Water Law, which transposes the main European water legislation - the Water Framework Directive.

The Water Framework Directive (WFD), transposed into the national legal system by the Water Law establishes the framework for sustainable water management, highlighting the main environmental goal of achieving a "Good" status for all surface water and ground water bodies. [https://rea.apambiente.pt/content/state-surface-and-groundwater-bodies?language=en]

> O Sistema Nacional de Informação de Recursos Hídricos (SNIRH) processa, valida e divulga toda a informação recolhida nas redes de monitorização da APA e de outras entidades (locais e regionais – Açores e Madeira).[https://apambiente.pt/agua/sistema-nacional-de-informacao-de-recursos-hidricos-snirh]

The National Water Resources Information System (SNIRH) processes, validates and publishes all the information collected in the APA monitoring networks and those of other bodies (local and regional - Azores and Madeira).[https://apambiente.pt/agua/sistema-nacional-de-informacao-de-recursos-hidricos-snirh]

> O SNIRH contribui para atingir, entre outros, os seguintes objetivos: avaliação das disponibilidades hídricas; caracterização dos recursos hídricos; avaliação da evolução da qualidade da água;[https://apambiente.pt/agua/sistema-nacional-de-informacao-de-recursos-hidricos-snirh]

The SNIRH helps achieve the following objectives, among others: assessment of water availability; characterisation of water resources; assessment of water quality trends 

The snirh covers the the alentejo region and wich includes the monitoring of EFMA.

Along over The EDIA and APA water quality monitoring database, there is available  early ambiental impact studys from ERSHA (Estudo dos Recursos Hídricos Subterrâneos do Alentejo) over this region  (1997-2001) [70658105.pdf] .   the study of this aquifer system was the responsibility of INAG (*Water Institute* of *Portugal*) [Redes de monitorizacoa -Alentejo  - https://snirh.apambiente.pt/snirh/download/relatorios/redes_agsub_alentejo.pdf], now part of APA since 2012 [https://www.apambiente.pt/apa]. 



