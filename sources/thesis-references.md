

####	[References](https://libguides.nps.edu/citation/ieee#fact-sheet)

EU WATER PROTECTION AND MANAGEMENT - **eu:water-protection**

EU Monitoring of waters - **eea:monitoring-waters**

surface water quality monitoring - European Env Assoc - **Kristensen1996**

European Env Assoc - Introduction and overview od monitoring - **Kristensen1996**

SpringOpen Future water quality - **Altenburger2019**

L. Beck, T. Bernauer - **Beck2010**

_____

Extended%20Abstract_AMG.pdf - **Coimbra**

https://www.edia.pt/en/alqueva/ - **edia:alqueva**

https://www.edia.pt/en/alqueva/the-territory/ - **edia:alqueva-territory**

/2005IAIA_AlquevaDam.pdf - **Melo2005**

From Estudo De Impacte Ambiental Do - **eia:pisao**

From Estudo de Impacte Ambiental dos Adutores de Pedrógão, Brinches-Enxoé e Serpa - **NEMUS2008**

RNT1439 - eia:alvito

https://www.edia.pt/en/what-we-do/construction-of-infrastructures/ - **edia:wwd-construction**

https://www.edia.pt/en/social-responsability/environmental-policy/ - edia:social-policy

https://www.edia.pt/en/what-we-do/monitoring/ - edia:wwd-monitoring

https://www.edia.pt/pt/o-que-fazemos/monitorizacao/agua/ - edia:quem-somos-agua

https://apambiente.pt/en/apa/portuguese-environment-agency-apa -  apa:en-about

https://rea.apambiente.pt/content/state-surface-and-groundwater-bodies?language=en - apa:rea-water

https://apambiente.pt/agua/sistema-nacional-de-informacao-de-recursos-hidricos-snirh - apa:snirh

70658105.pdf - **AntonioReisRosadoParalta2009**

M00940 -  **eia:inf12**

Redes de monitorizacoa -Alentejo  - https://snirh.apambiente.pt/snirh/download/relatorios/redes_agsub_alentejo.pdf  - inag:erhsa

https://www.apambiente.pt/apa - apa

