### 1  introduction

#### 	Motivation



Surface streams and rivers are important sources of fresh water for domestic, industrial, and irrigation. The water quality control turns out as essencial to water management policy on promoting sustainable  use of water  and safe-water to human consumption [health-waterquality-en.pdf].  The water quality monitoring provides quantitative information about characteristics of water  allowing to identifiyng and acting  in order to ensure the human health and protect  ecosystems [EU Monitoring of waters].  The European Union established ambiental directives to protect water and  preserving this vital resource to future generations. 

The Drinking Water Directive  requires that Member States to regularly monitor the quality of water intended for human consumption by using a ‘sampling points’.  This EU's  risk-based water safety assessment was created to  identify and address possible risks to water sources already at the distribution level.

Urban Waste Water Treatment and Nitrates directives are others examples of measures  implemented on EU state members, Both the directive and the regulation aim to safeguard drinking water and prevent damage from eutrophication. The Urban Waste Water Treatment directive objective is protect the environment from the adverse effects of urban waste water discharges and discharges from industry  by setting minimum standards ,  treatment and discharge of urban waste water, introduces controls on the disposal of sewage sludge and applying others regulations. 

The Nitrates directives is focused on protect waters from nitrates from agricultural sources. A complementary regulation requires Member States to send a report to the Commission providing details of codes of good agricultural practice, designated nitrate vulnerable zones (NVZ), water monitoring and others [EU WATER PROTECTION AND MANAGEMENT] .



The type of information depends on the objectives and purposes of the monitoring programme, such as detection of drinking water standard violations or assessment of water quality trends [EU Monitoring of waters] . The number of determinands which describe water quality is continuously increasing. Moreover, determinands are constantly being modified and refined in line with the expanding uses to which water is being put and in pace with the development of analytical capabilities for measuring more substances at ever lower concentrations. [EU-Introduction and overview of monitoring activities]. With help of the emerging technologies and  latest research works, the future water monitoring systems will help to collect more complex chemical mixtures and new relevant determinants  for  more specific and detailed water quality analysis. Monitoring serves a range of purposes, from control of chemical and ecological status compliance to safeguarding specific water uses, such as drinking water abstraction.[SpringerOpen Future water quality]. 

The chanllage appears on how to represent complex data with  spatial and temporal dimension,  how to aggreated data from decades of monitoring campaings  with different range of purposes and differnte standardizations[L. Beck, T. Bernauer]. Also this leads how to design a flexible framework   to  adapt  to future standards for monitoring of water contamination. 







_____





**From European Parliament - WATER PROTECTION AND MANAGEMENT** https://www.europarl.europa.eu/factsheets/en/sheet/74/water-protection-and-management 



The revised [Drinking Water Directive](https://eur-lex.europa.eu/eli/dir/2020/2184/oj) of 2020 defines essential quality standards for water intended for human consumption. It requires Member States to regularly monitor the quality of water intended for human consumption by using a ‘sampling points’ method. An EU-wide risk-based water safety assessment will help to identify and address possible risks to water sources already at the distribution level.



The [Urban Waste Water Treatment Directive](http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=celex%3A31991L0271) aims to protect the environment from the adverse effects of urban waste water discharges and discharges from industry. The directive sets minimum standards and timetables for the collection, treatment and discharge of urban waste water, introduces controls on the disposal of sewage sludge, and requires the dumping of sewage sludge at sea to be phased out.

The Commission is intending to update this directive, to better counter water scarcity by facilitating the reuse of treated waste water for agricultural irrigation. Following a [public consultation](https://ec.europa.eu/info/law/better-regulation/have-your-say/initiatives/12405-Water-pollution-EU-rules-on-urban-wastewater-treatment-update-) in the first quarter of 2021, the Commission intends to adopt its proposal for a revised directive in the first quarter of 2022.



The [Nitrates Directive](http://eur-lex.europa.eu/legal-content/en/ALL/?uri=CELEX:31991L0676) aims to protect waters from nitrates from agricultural sources. A complementary regulation requires Member States to send a report to the Commission every four years, providing details of codes of good agricultural practice, designated nitrate vulnerable zones (NVZ), water monitoring and a summary of action programmes. Both the directive and the regulation aim to safeguard drinking water and prevent damage from eutrophication







**From Eu Enviroment Agency - Monitoring of waters** https://www.eea.europa.eu/archived/archived-content-water-topic/status-and-monitoring/monitoring-of-waters/monitoring-of-waters

The objective of water quality monitoring is to obtain quantitative information on the physical, chemical and biological characteristics of water via statistical sampling. The type of information sought depends on the objectives and purposes of the monitoring programme, such as detection of drinking water standard violations or assessment of water quality trends.



**From Eu Enviroment Agency -Introduction and overview of monitoring activities** https://www.eea.europa.eu/archived/archived-content-water-topic/status-and-monitoring/monitoring-of-waters/introduction-and-overview-of-monitoring-activities

Determinands measured The number of determinands which describe water quality is continuously increasing. Moreover, determinands are constantly being modified and refined in line with the expanding uses to which water is being put and in pace with the development of analytical capabilities for measuring more substances at ever lower concentrations.





**From SpringerOpen Future water quality monitoring: ** https://enveurope.springeropen.com/articles/10.1186/s12302-019-0193-1



Environmental water quality monitoring aims to provide the data required for safeguarding the environment against adverse biological effects from multiple chemical contamination arising from anthropogenic diffuse emissions and point sources. Here, we integrate the experience of the international EU-funded project SOLUTIONS to shift the focus of water monitoring from a few legacy chemicals to complex chemical mixtures, and to identify relevant drivers of toxic effects. Monitoring serves a range of purposes, from control of chemical and ecological status compliance to safeguarding specific water uses, such as drinking water abstraction. Various water sampling techniques, chemical target, suspect and non-target analyses as well as an array of in vitro, in vivo and in situ bioanalytical methods were advanced to improve monitoring of water contamination. 



**From copy-paste articles**

*Water and conservation topic, why is important  and motivation*

The  water quality control is the important method to ensure safe water to user-consume, to 

Surface streams and rivers are important sources of fresh water for domestic, industrial, and irrigation
purposes in Kenya. The rapid rise of commercial horticulture [1–5] has impaired water quality, affecting the physico-chemical properties of surface waters [6–8]. Horticultural pollutants transported in runoff or through leaching into surface streams contribute to declining water quality that limits stream integrity and usefulness. The management of horticultural pollutants in watersheds with diverse land uses [9–11] is a complex process, and is often faced with the difficulty of separating effluents from different land processes and land use types. In addition, horticultural effluents are high in nitrogen and phosphorus, pesticide residues, and soil enhancements [7], which degrade water quality , making it unhealthy for human use, while also exposing the aquatic communities to habitation stress.

At present,
hydrological models are mainly divided into conceptual models and physically based distributed models. Conceptual hydrological models generalize runoff generation and runoff routing processes based on the mass conservation and experimental simulation or empirical function relationship. The conceptual model is simple and easy to be established, but the parameters are difficult to be determined by the theoretical formula. Generally,

In the spatial sciences geographic information systems (GIS) are used to computationally model, simulate, and analyze these processes and their impact on the landscape. Similarly


The infiltration ofuntreatedwastewater into aquifers highly endangers the availability offresh-water for human consumption in semi-arid areas. This growing problem ofpotable water scarcity urgently requires solutions for groundwater protection. Decision support systems for local wastewater treatments in settlements already exist. However, the main challenge of implementing these for regional groundwater protection is to identify where wastewater treatments are most efficient for the whole region.

Hydrological and water quality models have been used largely in environmental studies for
identifying sources of pollutants and developing scenarios in order to test environmental measures to Water reduce pollution or mitigate climate change impacts on water resources

**From Wikipedia Strahler** https://en.wikipedia.org/wiki/Strahler_number



_________



#### 1.1  Background

*EDIA (sistemas de barragens e tranvazes e campos)* 

*Tamanho dos sistemas, barragens, reservatorios, datas do project - ver site EDIA*

- **From EDIA website https://www.edia.pt/pt/quem-somos/** 

  - A EDIA pertencente ao Setor Empresarial do Estado (SEE), é a empresa gestora do Empreendimento de Fins Múltiplos de Alqueva.

    Sendo responsável por um instrumento relevante para dinamização da economia, posiciona-se como uma referência estratégica, contribuindo para o desenvolvimento, não só da região, mas também do País.

    Até ao encerramento das comportas da barragem de Alqueva,em fevereiro de 2002 e consequente enchimento da sua albufeira, a EDIA afirmou-se como a Empresa garante da construção das infraestruturas.

    Consciente do papel que assumia na região e com o objetivo de associar às infraestruturas do Projeto de Alqueva polos de desenvolvimento, apostou na perspetiva empresarial na sua orientação.

    Hoje a EDIA é reconhecida a nível nacional e, também além-fronteiras, como uma Empresa sólida e estratégica para a promoção de Alqueva, rentabilizando a sua componente agrícola; para a promoção da região, enquanto zona de referência para novos investimentos; para o estabelecimento de pontes facilitadoras entre investidores e empresários locais, tendo em vista parcerias em diversas áreas de negócio, para além de ser responsável direta pela conceção, construção e exploração das infraestruturas que estão afetas ao Empreendimento de Fins Múltiplos de Alqueva.



- **From: Massas de água de Alqueva**

  - O presente trabalho pretende compilar informação associada às albufeiras pertencentes ao Empreendimento de Fins Múltiplos de Alqueva (EFMA), referindo as caraterísticas principais quer da barragem, quer da massa de água. São ainda referidos os principais resultados em termos de monitorização ambiental, com enfoque na qualidade da água para rega, sinalização da albufeira e utilizações secundárias permitidas. Nesta primeira fase do trabalho foram selecionadas 16 albufeiras: Alqueva, Pedrogão, Álamos I, II e III, Cinco Reis, Loureiro, Penedrão, Pisão, São Pedro, Amoreira, Brinches, Caliços, Laje, Pias e Serpa. Estas albufeiras encontram-se agrupadas em três subsistemas de rega: Alqueva, Pedrogão e Ardila, de acordo com a principal origem de água. Posteriormente este documento será atualizado por forma a incluir as restantes albufeiras integradas no EFMA, como é o caso das albufeiras de Furta-Galinhas, Magra e Almeidas

    Localizado no Alentejo, o EFMA tem influencia direta nos concelhos abrangidos pelas novas albufeiras e naqueles que beneficiam com a instalação dos perímetros de rega ou são servidos pelo reforço do abastecimento público. O EFMA é um projeto centrado na barragem de Alqueva, a maior reserva estratégica de água da Europa. A partir de Alqueva, interligam-se barragens garantindo a disponibilidade de água a uma área aproximada de 10 000 km2, divididos pelos distritos de Beja, Évora, Portalegre e Setúbal, abrangendo um total de 20 concelhos.

    O Sistema Global de Rega de Alqueva beneficia uma área com cerca de 120 000 hectares. É constituído por um total de 69 barragens, reservatórios e açudes, 382 km de rede primária, 1 620 km de extensão de condutas na rede secundária, 47 estações elevatórias, 5 centrais mini-hídricas e 1 central fotovoltaica. 

    O Sistema Global de Rega do EFMA divide-se em três subsistemas, de acordo com as diferentes origens de água: Alqueva, Pedrógão e Ardila, com base nas duas albufeiras principais Alqueva e Pedrógão.

    Qualidade da Água
    O programa de monitorização de qualidade da água na rede primária do EFMA visa: • Recolher dados de suporte à gestão e exploração do EFMA; • Avaliar a adequabilidade da água captada para o uso rega; • Avaliar a adequabilidade do caudal ecológico libertado para jusante; • Assegurar o cumprimento dos diplomas legais em vigor, face às responsabilidades da EDIA; • Assegurar o cumprimento dos compromissos ambientais da responsabilidade

     

- **From M00940**

  - No âmbito da implementação do visitam global de rega do Empreendimento de fins Múltiplos de Aqueva, a EDIA - Empresa de Desenvolvimento e Infra-estruturas do Alqueva, S.A., 

- **From Estudo De Impacte Ambiental Do**

  - O promotor do projecto é a EDIA – Empresa de Desenvolvimento e Infra-estruturas do Alqueva, S. A., empresa responsável pela construção e exploração do Empreendimento de Fins Múltiplos do Alqueva.
  - Na área de influência do projecto em estudo, os recursos hídricos disponíveis, quer superficiais, quer subterrâneos são maioritariamente afectos à agricultura. O abastecimento humano é fundamentalmente assegurado por fontes superficiais,

  _______

  §

##### *Poluicao* 

​     *Agropecuaria* 

​     *Saneamento* 

​     *Indurstria* 

- **From  M00940**
  - Domina a actividade agricola em que o montado e as exploracoes extensivas de sequeiro determinam uma paisagem de campos aberto. A existencias de quequenas áreas de culturas horticulas e horto-industrieais. o caráter detrítico dos terrenos reflecte a grande vulnerabilidade à poluição a que as águas subterrâneas estão sujeitas. As características dos terrenos favorecem a rápida infiltração das aguas e dos contaminantes arrastados até ao nivel freático, conferindo-lhes condições de desfavoraveis à depuracao dos poluentes devido à grande velocidade com que se processa o fluxo em profundidade .  O escoamento superficial dos terrenos e os produtos associados à actividade agriicol afectarão directamente a qualidade das águas das linhas de água que ainda mantêm uma galeria ripícola muito desenvolvida e deversas espécies aquáticas, associadas ao meio ou favorecidads pela proxximidade às linhas de água. Em termos gerais as aguas reflectem a contaminação de origem agricola com valores superiores ao valor máximo recomendável para os fosfatos, azoto e nitratos e. organica devido essencialmente a explorações agropecuárias.
-  **From Estudo De Impacte Ambiental Do**
  - As principais fontes de poluição na área em análise referem-se às descargas de águas residuais domésticas tratadas nos sistemas de tratamento dos vários aglomerados populacionais da área de análise, às águas residuais de algumas instalações de bovinicultura e suinicultura e, por último, à poluição resultante das práticas agrícolas. Os dados de qualidade disponíveis reflectem bem esta situação, verificando-se, por exemplo, que a água da albufeira do Roxo apresenta alguns parâmetros em níveis de qualidade não aceitáveis, principalmente os que se relacionam com fontes de contaminação orgânica e as águas do sistema aquífero Gabros de Beja evidenciam qualidade deficiente no que respeita às concentrações de nitratos.

- **From Estudo de Impacte Ambiental dos Adutores de Pedrógão, Brinches-Enxoé e Serpa**
  -  as águas subterrâneas extraídas nestas captações apresentam alguns problemas de qualidade relacionados com as altas concentrações do ião nitrato.
  - No que diz respeito aos resíduos e efluentes, o concelho de Serpa apresenta um índice de recolha de resíduos sólidos urbanos relativamente elevado, mas uma baixa taxa de recolha selectiva. Os resíduos predominantes são os de origem doméstica e agrícola.Ao nível dos efluentes líquidos, o concelho de Serpa apresenta um baixo índice de tratamento, com apenas 47% de população servida com ETAR em funcionamento.





________



##### 

#####  *legislação/regulamantacao*

**From APRH** https://www.aprh.pt/pt/jt-out-2011/pdf/35_apresentacao.pdf

- A Lei da Água (LA - Lei n.º 58/2005, de 29 de Dezembro) transpôs para a ordem jurídica nacional a Diretiva Quadro da Água (DQA - Diretiva 2000/60/CE, do Parlamento Europeu e do Conselho, de 23 de Outubro), que estabelece um quadro de ação comunitária no domínio da política da água. Tem por objectivo proteger as massas de água superficiais interiores, costeiras e de transição, e subterrâneas. A DQA/ LA estipula como objectivos ambientais o bom estado, ou o bom potencial, das massas de água, que devem ser atingidos até 2015, através da aplicação dos programas de medidas especificados nos planos de gestão das regiões hidrográficas. A região hidrográfica, constituída por uma ou mais bacias hidrográficas, é a unidade territorial de gestão da água. A competência para elaboração dos planos de gestão de região hidrográfica, enquanto instrumentos de planeamento dos recursos hídricos que visam a gestão, a proteção e a valorização ambiental, social e económica das águas ao nível das bacias hidrográficas integradas numa região hidrográfica, está cometida à Agência Portuguesa do Ambiente, I.P. Os PGRH estão sujeitos ao parecer do Conselho de Região Hidrográfica e à aprovação da Autoridade Nacional da Água (APA, I.P.) 

**From 35 powerpoint**

- ​	A avaliação e verificação da conformidade da qualidade da água para o uso rega teve por base os critérios definidos no Decreto-Lei nº 236/98 de 1 de Agosto.



**From APA ambiente **https://apambiente.pt/agua/legislacao

- A APA, como Autoridade Nacional da Água, tem competências no desenvolvimento e na implementação deste quadro legal, nomeadamente da [Lei da Água](https://apambiente.pt/agua/lei-da-agua), que transpõe a principal legislação Europeia relativa à água - a [Diretiva Quadro da Água](https://eur-lex.europa.eu/resource.html?uri=cellar:5c835afb-2ec6-4577-bdf8-756d3d694eeb.0009.02/DOC_1&format=PDF). 

- A **Lei da Água**, que transpõe para a ordem jurídica nacional a **Diretiva Quadro da Água** ([Diretiva 2000/60/CE](http://eur-lex.europa.eu/resource.html?uri=cellar:5c835afb-2ec6-4577-bdf8-756d3d694eeb.0009.02/DOC_1&format=PDF) do Parlamento Europeu e do Conselho, de 23 de outubro de 2000) é estabelecida pela [Lei n.º 58/2005](https://dre.pt/application/file/469114), de 29 de dezembro, alterada pelos Decretos-Lei n.ºs [245/2009](https://dre.pt/application/file/a/490347), de 22 de setembro; [60/2012](https://dre.pt/application/file/a/553481), de 14 de março e [130/2012](https://dre.pt/application/file/178471), de 22 de junho e pelas Leis n.º [42/2016](https://dre.pt/application/file/a/75058349), de 28 de dezembro e n.º [44/2017](https://dre.pt/application/file/a/107515162), de 19 de junho.

  São objetivos da **Lei da Água**:

  - Evitar a continuação da degradação e proteger e melhorar o estado dos ecossistemas aquáticos e também dos ecossistemas terrestres e zonas húmidas diretamente dependentes dos ecossistemas aquáticos, no que respeita às suas necessidades de água;
  - Promover uma utilização sustentável de água, baseada numa proteção a longo prazo dos recursos hídricos disponíveis;
  - Obter uma proteção reforçada e um melhoramento do ambiente aquático, nomeadamente através de medidas específicas para a redução gradual e a cessação ou eliminação por fases das descargas, das emissões e perdas de substâncias prioritárias;
  - Assegurar a redução gradual da poluição das águas subterrâneas e evitar o agravamento da sua poluição;
  - Mitigar os efeitos das inundações e das secas;
  - Assegurar o fornecimento em quantidade suficiente de água de origem superficial e subterrânea de boa qualidade, conforme necessário para uma utilização sustentável, equilibrada e equitativa da água;
  - Proteger as águas marinhas, incluindo as territoriais;
  - Assegurar o cumprimento dos objetivos dos acordos internacionais pertinentes, incluindo os que se destinam à prevenção e eliminação da poluição no ambiente marinho.

  A **Lei da Água** define quais as utilizações privativas dos recursos hídricos sujeitas a licenciamento. O **Regime da Utilização dos Recursos Hídricos** está estabelecido no [Decreto-Lei n.º 226-A/2007](https://dre.pt/application/file/a/340313), de 31 de maio, na sua redação atual.

  -

_______



#####    *Monitorização de qualidade da água*  

- **From APA**  https://rea.apambiente.pt/content/press%C3%B5es-quantitativas-e-qualitativas-sobre-os-recursos-h%C3%ADdricos 

  A estimativa dos valores de carga bruta de azoto (N) e de fósforo (P) gerados pela atividade pecuária iniciou-se com a obtenção da quantidade média de nutrientes excretados anualmente por “cabeça normal” (CN) para cada espécie pecuária. 

  Os valores de CN foram definidos no [Decreto-Lei n.º 214/2008](https://dre.pt/application/file/439340) e o número e a espécie/tipo de animal existente em cada uma das explorações obteve-se com base nos dados do Recenseamento Agrícola de 2009 (RA 2009), disponibilizados pelo Instituto Nacional de Estatística (INE).

  A carga total gerada em cada uma das explorações foi avaliada tendo como base a quantidade média de azoto total e de fosfatos (P2O5) excretados anualmente por CN, definida na [Portaria n.º 259/2012](https://dre.pt/application/file/174644).



​      *EDIA (+- [rivado)* 

- **From EDIA Website https://www.edia.pt/pt/o-que-fazemos/monitorizacao/agua/**

  - A EDIA, enquanto entidade gestora do Empreendimento de Fins Múltiplos de Alqueva (EFMA) promove a implementação de um conjunto de programas de monitorização, sendo responsável pelas seguintes ações:

    - Implementação e exploração de novas estações climatológicas e hidrométricas, no âmbito da gestão do EFMA;
    - Monitorização do estado das massas de águas superficiais e subterrâneas durante as fases de construção e exploração do Empreendimento.

    A monitorização das massas de água tem como objetivos:

    - Avaliar a adequabilidade da água em trânsito no sistema e a sua adaptabilidade aos usos previstos no Contrato de Concessão, celebrado entre o Estado Português e a EDIA;
    - Integrar as disposições de monitorização resultantes dos diplomas legais em vigor, face às responsabilidades da EDIA;
    - Recolher os dados de suporte à tomada de decisão, com vista à gestão e exploração do EFMA;
    - Avaliar a eficácia dos caudais ecológicos e de outras medidas de mitigação implementadas, ou a implementar;
    - Salvaguardar a EDIA da responsabilidade de uma eventual degradação da qualidade da água decorrente de ações promovidas por terceiros;
    - Avaliar os potenciais impactes da transferência de água entre as albufeiras do Loureiro e Alvito;
    - Avaliar os potenciais impactos das escorrências agrícolas sobre os recursos hídricos superficiais e subterrâneos
    - Fonte: Estações Automáticas EDIA/CPPE
    - **From Auditoria de Gestão À EDIA, SA ( Alqueva )**
      - Acordo entre a EDIA e a EDP, em representação da Companhia Portuguesa de Produção de Energia Eléctrica (CPPE) . a EDIA, na qualidade de gestora e concessionária do Aproveitamento Hidroeléctrico, transferirá, para a CPPE, o direito de exploração da Central, para produção de energia eléctrica, por um prazo de 30 anos, como contrapartida da participação desta na execução do Projecto. Neste contexto, a CPPE participará nos custos de investimento do Aproveitamento Hidroeléctrico pelo montante da denominada “valia eléctrica liquida”, definida nos termos do Anexo ao Protocolo. 

  - **From 35 powerpoint**

    - Estações Automáticas de Monitorização Constitui o Sistema de Vigilância e Alerta do Sistema Alqueva-Pedrógão, origem de água de todo o EFMA. Para além de um conjunto de estações instaladas pela EDIA o sistema engloba estações integradas na Rede Nacional de Monitorização dos Recursos Hídricos. Todas as estações de qualidade da água localizadas nas albufeiras estão dotadas de sondas para monitorização a diversas profundidades, tendo a EDIA promovido a instalação do equipamento necessário nas estações da rede Nacional.



​      *APA (+- publico)* 

- **From APA Website**

  - Bem-vindos ao novo site da APA - Agência Portuguesa do Ambiente!

    Esta é a **agência do Estado** que tem como missão a gestão integrada das políticas ambientais e de sustentabilidade.A **APA** tem competências de monitorização, planeamento e avaliação, licenciamento e fiscalização, sendo por isso o principal regulador ambiental em Portugal.

  - https://www.apambiente.pt/agua O papel da APA, enquanto **Autoridade Nacional da Água**, é definir políticas e instrumentos de gestão que assegurem a aplicação destes princípios. Este papel é partilhado com outras entidades – como a ERSAR (Entidade Reguladora dos Serviços de Água e Resíduos) – que regula o abastecimento de água para consumo humano e o saneamento de águas residuais urbanas.

    A gestão das regiões hidrográficas é materializada no terreno através de serviços desconcentrados no território: as **Administrações de Região Hidrográfica** do Norte, Centro, Tejo e Oeste, Alentejo e Algarve.

    Com vista a assegurar uma gestão sustentável da água e a proteção dos recursos hídricos, a APA desenvolve um vasto conjunto de atividades que incluem a definição e execução da política nacional de recursos hídricos, o planeamento e ordenamento destes recursos e do território associado**,** o licenciamento da sua utilização e respetiva fiscalização, a promoção do uso eficiente da água, a implementação de programas de monitorização e a aplicação da taxa de recursos hídricos.

  - https://apambiente.pt/agua/sistema-nacional-de-informacao-de-recursos-hidricos-snirh O Sistema Nacional de Informação de Recursos Hídricos (SNIRH) processa, valida e divulga toda a informação recolhida nas redes de monitorização da APA e de outras entidades (locais e regionais – Açores e Madeira).

    Tem por missão manter o desenvolvimento da estrutura tecnológica de carregamento, validação e disponibilização ao público de dados (hidrometeorológicos, qualidade da água, ecológicos e ambientais), informação e conhecimento sobre águas interiores (superficiais e subterrâneas), costeiras e de transição e de promover conteúdos educativos adaptados aos jovens, com funcionalidades acrescidas para cidadãos com necessidades especiais.

    Tem também atribuições no domínio do apoio técnico especializado (para universitários, consultores, entidades judiciais, Ministérios, órgãos da Comissão Europeia, entre outros) e está preparado para responder às necessidades dos países de língua oficial portuguesa (disponibilização de informação e apoio técnico).

    O SNIRH contribui para atingir, entre outros, os seguintes objetivos: avaliação das disponibilidades hídricas; caracterização dos recursos hídricos; avaliação da evolução da qualidade da água; avaliação das situações extremas de secas e cheias; identificação de acidentes de poluição; avaliação e análise de impactes sobre os meios hídricos; avaliação de programas de medidas; verificação de normativos; avaliação de projetos hidráulicos; delimitação do domínio público hídrico; disponibilização de dados e informação que permitem o desenvolvimento de diversos estudos bem como de apoio ao planeamento e gestão de recursos hídricos.

- **From Rede comum conhecimento website**:
  - SNIRH - Sistema de Vigilância e Alerta de Recursos Hídricos
  - Estudo dos Recursos Hídricos Subterrâneos do Alentejo (ERSHA) (2000).

_________



#### 1.2 Problem Statement

the problem itself, stated clearly and with enough contextual detail to establish why it is important;  

A problem statement is usually one or two sentences to explain the problem your process improvement project will address. In general, a problem statement will outline the negative points of the current situation and explain why this matters. It also serves as a great communication tool, helping to get buy­in and support from others.



Example 1

[problem and its context] `A recent trend in the design of new aircraft is the addition of winglets, which are small fins attached to the ends of the main wing. After an aircraft has taken off and is cruising, winglets improve its performance by reducing the drag caused by the main wing. However, during the critical stages of aircraft takeoff and landing, the winglets cause two problems. First, they cause vibrations in the main wing, commonly called buffeting. Second, they cause the aircraft to lose some control of yaw, the motion of the nose right and left. In a study funded by NASA [Ref. 2], the main wing of a DC-10 transport aircraft was outfitted with winglets, and it experienced significant buffeting during takeoff and landing.`



[approach of the current research] `In our current project, we examine winglet-induced buffeting in three wing designs. We record buffeting and yaw under experimental wind-tunnel takeoff and landing conditions for (1) a wing without winglets, (2) another wing with conventional winglets, and (3) a wing with spheroid winglets. Our objective is to determine the degree to which differences between load lifts on the wings and their winglets during takeoff and landing are causing the performance problems we have described.`



Example 2

The staffing model in the Process Improvement Unit (PIU) has changed (we have more staff, and some of the staff have different working patterns) we need to have a clear way of recording status and stage of our business activities (projects, workshops and training) that will be used by all PIU staff, so that we can work effectively and provide good service to our customers. A member of staff is due to go on annual leave in two weeks time and we have no visibility or way of easily sharing information about their work, this will make it hard for the rest of the team to cover the work during staff absence.



























***2 redes de monitorização Acesso a dados +- independentes***     

***Tamanho dos dados***:

- *N. de estacaoes*
- *Tempo* 
- *Poluentes* 

***Impossivel relacionar dados:*** 

- *Num "rbiero"não há informacao de flow*
- *Nao infomrcao dos trnsvazes* 
- *Impossivel vfazer qq tipo de analize espacio tempora dos dados recolhidos com outra informação complementar:* 
- *Relacoes temporais no mesmo ponto* 
- *Relacoes topologicas ao longo dos canai* 
- *Relacéos com outros factore de poluica (industria, uso do solo)* 

###  

### 1.3  Proposed Solution

***Plataforma par agregação dod dodos dede poluicoa*** 

***Definição do models/formatos de dados para representação de:*** 

- *Infraestrutura de* 

- *Estacoes qualidade* 
- *Resulytadso de analises de agua* 
- *Armaenamento de dados em grafos e paltaforma para exploração de dados.* 

***Explicar pq é bom o serviço*** 

***Producao dos grafos com base nas coordenadas dos pontos relevantes*** 

***Automatizaçao das listagem de poluente avaliados*** 

***Interface de visual data exploration***   



### 2	State of the art / Related work 

#### 	2.1 - Melhor definciçao do ambiente 

​	*EDIA* 

​	*Fontes de poluicao* 

​	*Portais/ fontes de dados* 



 

#### 	2.2 - Db graphos 

​	*Deteçao de poluentes* 

 

#### 	2.3 - poluicao 

 

### 3 - Data Modeling

#### 3.1 – formatos dos dados de input 

#### 3.2 - represtaçao interna 

   *Descrbver os grafos* 

 

### 4 – Framework 

#### 4.1- objectivos 

*Integrar num só sistema todos os dados de poluicao* 

*Permitir processamentos mais avanaçados ("geo estatistica")* 

*Permitir a exploração dos dados* 

#### 4.2 - Arquitecture 

 

#### 4.3 Data Model

 

#### 4.4 implementation

*Tecnologias e porquê* 

##### 4.4.1 - modulo 1 

##### 4.4.2 = module 2 

 

#### 4.5 - Demonstration 

- *instalação* , CI/CD
- *Numero de nós e relacoes* 
- *Funcionalidades data exploration* 
- **    

### 5 – Conclusion 

*1 plataforma* 

*Dados concistentes* 

*Dadao srelacionadaos* 

** 

*Prova de conceito – visual dat explorer que permite:* 

*Dados ao ongo do rio* 

*Dados ao longo do tempo* 

** 

*Trabalho futuro* 

*Bugs* 

*Nova funcionalidae* 

*Trabalho futruro que só agora se consegue fazer (ML sobre este tipo de "ambientes"* 

 